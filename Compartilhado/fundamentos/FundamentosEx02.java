package logica.Aula7.fundamentos;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FundamentosEx02 extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JPanel panel;
	private JTable table;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JTextField txtHorasTrab;
	private JTextField txtSalarioHora;
	private JLabel lblNewLabel_2;
	private JTextField txtQtdDependente;
	private JButton btnCalcular;
	private JButton btnSair;
	JFrame aTela;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FundamentosEx02 frame = new FundamentosEx02();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FundamentosEx02() {
		//this.aTela =  aTela;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 613, 406);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 591, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(39, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 496, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(24, Short.MAX_VALUE))
		);
		
		table = new JTable();
		
		lblNewLabel = new JLabel("Qtde de horas trabalhadas");
		
		lblNewLabel_1 = new JLabel("Salário horas");
		
		txtHorasTrab = new JTextField();
		txtHorasTrab.setColumns(10);
		
		txtSalarioHora = new JTextField();
		txtSalarioHora.setColumns(10);
		
		lblNewLabel_2 = new JLabel("Número de Dependentes");
		
		txtQtdDependente = new JTextField();
		txtQtdDependente.setColumns(10);
		
		btnCalcular = new JButton("Calcular");
		//btnCalcular.addActionListener(this);
		
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				
			   double _horastrabalhadas = Float.parseFloat(txtHorasTrab.getText());
			   double _salariohora = Float.parseFloat(txtSalarioHora.getText());;
			   int _qtddependente = Integer.parseInt(txtQtdDependente.getText()); 
		       double _salariobruto = _horastrabalhadas * _salariohora + (50 * _qtddependente);
		       double _inss = 0;
		       double _ir = 0;
		       double _salarioliquido = 0;
		       
		       if (_salariobruto > 1000) 
		    	   _inss = _salariobruto * 9/100;		       
		       else 
		    	   _inss = _salariobruto * 8.5/100;
		       
		       if (_salariobruto > 1000) 
		    	   _ir = _salariobruto * 7/100;
		       else if  (_salariobruto > 500)
		    	   _ir = _salariobruto * 5/100;
		       else 
		    	   _ir = 0;
		       
		       _salarioliquido = _salariobruto - _inss - _ir;
			   String _msg;
			   _msg = String.format("Salário Bruto R$ %,.2f\n",_salariobruto);
		       _msg += String.format("Inss R$ %,.2f\n",_inss);
		       _msg += String.format("IR R$ %,.2f\n",_ir);
		       _msg += String.format("Salário Líquido R$ %,.2f\n",_salarioliquido);
		       
		       JOptionPane.showMessageDialog(aTela,_msg);
		       

		       
		       DefaultTableModel dtm = new DefaultTableModel(0, 0);

		      // add header of the table
		      String header[] = new String[] { "Descrição", "Valor" };

		      // add header in table model     
		       dtm.setColumnIdentifiers(header);
		        
		       table.setModel(dtm);

		       
		       // add row dynamically into the table
		       dtm.addRow(new Object[] { "Qtde de horas trabalhadas", String.format("%,.1f",_horastrabalhadas) } );
		       
		       dtm.addRow(new Object[] { "Salário Hora", String.format("R$ %,.2f",_salariohora) } );
		       dtm.addRow(new Object[] { "Qtde de dependente", String.format("%d",_qtddependente) } );
		       dtm.addRow(new Object[] { "Salário Bruto", String.format("R$ %,.2f",_salariobruto) } );
		       dtm.addRow(new Object[] {"Inss", String.format("R$ %,.2f",_inss)} );
		       dtm.addRow(new Object[] {"IR", String.format("R$ %,.2f",_ir)} );
		       dtm.addRow(new Object[] {"Salário Líquido",String.format("R$ %,.2f",_salarioliquido)});
			}
		});
		
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(35)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel)
								.addComponent(lblNewLabel_1)
								.addComponent(lblNewLabel_2))
							.addGap(18)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(txtQtdDependente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtSalarioHora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(txtHorasTrab, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(52)
									.addComponent(btnCalcular)
									.addGap(40)
									.addComponent(btnSair))))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(27)
							.addComponent(table, GroupLayout.PREFERRED_SIZE, 522, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(42, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(txtHorasTrab, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCalcular)
						.addComponent(btnSair))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(txtSalarioHora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtQtdDependente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_2))
					.addGap(18)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(170, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}
	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}
}
