package logica.Aula7.fundamentos;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class FundamentosEx01 extends JFrame implements ActionListener {

	double _valortotal = 0;
    String _msg ="";
    
	private JPanel contentPane;
	private JLabel lblNewLabel;
	private JTextField txtQuantidade;
	private JLabel lblNewLabel_1;
	private JTextField txtValor;
	private JButton btnAdicionarValor;
	private JButton btnSair;
	private JButton btnLimparTotal;
	private JLabel lblValorTotal;
	private JTextField txtValorTotal;
	private JButton btnExbir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FundamentosEx01 frame = new FundamentosEx01();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FundamentosEx01() {
		setTitle("Exercícios Fundamentos 01");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 472, 210);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblNewLabel = new JLabel("Quantidade");
		
		txtQuantidade = new JTextField();
		txtQuantidade.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Valor R$");
		
		
		
		txtValor = new JTextField();
		txtValor.setColumns(10);
		
		btnAdicionarValor = new JButton("Adicionar ");
	//	btnAdicionarValor.addActionListener(this);
		btnAdicionarValor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				
				int quantidade = Integer.parseInt( txtQuantidade.getText()); 
				double valor = Float.parseFloat(txtValor.getText());
				_valortotal = _valortotal + quantidade * valor;
				_msg +=  "Quantidade " +quantidade+"\n";
				_msg +=  String.format("Valor: R$ %,.2f\n",valor);
				//txtValorTotal.setText(String.valueOf(_valortotal));				
				txtValorTotal.setText(String.format("%,.2f",_valortotal));
				txtQuantidade.setText("");
				txtValor.setText("");
				txtQuantidade.requestFocus();
				
			}
		});
		
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		
		btnLimparTotal = new JButton("Limpar Total");
		//btnLimparTotal.addActionListener(this);
		
		btnLimparTotal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				_valortotal = 0;
				_msg ="";
				txtQuantidade.setText("");
				txtValor.setText("");
				txtValorTotal.setText("");			
							}
		});
		
		lblValorTotal = new JLabel("Valor Total  R$");
		
		txtValorTotal = new JTextField();
		txtValorTotal.setRequestFocusEnabled(false);
		//txtValorTotal.setEnabled(false);
		txtValorTotal.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtValorTotal.setText("0,00");
		txtValorTotal.setForeground(Color.BLACK);
		txtValorTotal.setBackground(Color.ORANGE);
		txtValorTotal.setColumns(10);
		
		btnExbir = new JButton("Imprimir");
		//btnExbir.addActionListener(this);
		btnExbir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				JOptionPane.showMessageDialog(null,_msg+"\nValor Total R$ "+txtValorTotal.getText());
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblValorTotal)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtQuantidade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(lblNewLabel_1)
							.addGap(45))
						.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
							.addComponent(btnLimparTotal)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnAdicionarValor)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnExbir)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSair))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(txtValorTotal, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(17)
								.addComponent(txtValor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(96))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(txtQuantidade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtValor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblValorTotal)
						.addComponent(txtValorTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLimparTotal)
						.addComponent(btnAdicionarValor)
						.addComponent(btnExbir)
						.addComponent(btnSair))
					.addGap(25))
		);
		contentPane.setLayout(gl_contentPane);
	}
	public void actionPerformed(ActionEvent arg0) {
		System.exit(0);
	}
}
