package logica.Aula10.exerciciosGui3.model;

public class Endereco {
   String logradouro;
   String numero;
   String bairro;
   String cep;
   
   public String getLogradouro() {
	   return logradouro;
   }
   public void setLogradouro(String logradouro) {
	   this.logradouro = logradouro;
   }
   public String getNumero() {
	   return numero;
   }
   public void setNumero(String numero) {
	   this.numero = numero;
   }
   public String getBairro() {
	   return bairro;
   }
   public void setBairro(String bairro) {
	   this.bairro = bairro;
   }
   public String getCep() {
	   return cep;
   }
   public void setCep(String cep) {
	   this.cep = cep;
   }
   
   @Override
   public String toString() {
	   return  logradouro + ", " + numero + "\nBairro: " + bairro + ", CEP:" + cep ;
   }
}
