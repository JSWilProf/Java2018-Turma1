package logica.Aula10.exerciciosGui3.model;

import java.util.List;

public class Fornecedor {
   String nome;
   Endereco endereco;
   List<Produto> produtos;
   
   public String getNome() {
	   return nome;
   }
   public void setNome(String nome) {
	   this.nome = nome;
   }
   public Endereco getEndereco() {
	   return endereco;
   }
   public void setEndereco(Endereco endereco) {
	   this.endereco = endereco;
   }
   public List<Produto> getProdutos() {
	   return produtos;
   }
   public void setProdutos(List<Produto> produtos) {
	   this.produtos = produtos;
   }
   
   @Override
   public String toString() {
	   String dadosforn;
	   dadosforn = nome + "\nEndereco:" + endereco+"\n";
	   dadosforn += "------------------------------------------------------------\n";
	   dadosforn += "Produtos:\n";
	   for(Produto prod: produtos) {
		   dadosforn += "            " +prod +"\n";   
	   }
	   return dadosforn;
   }

}
