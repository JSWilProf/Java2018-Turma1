package logica.Aula10.exerciciosGui3.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import logica.Aula10.exerciciosGui3.model.Endereco;

import objetos.gui.lib.SwUtil;

public class CadEndereco extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JTextField tfLogradouro;
	private JLabel lblNumero;
	private JTextField tfNumero;
	private JLabel lblBairro;
	private JTextField tfBairro;
	private JFormattedTextField ftfCep;
	private JLabel lblCEP;
	private JButton btnSalvar;
	private JButton btnFechar;
	
	private Endereco endereco; 
		
	/** 
	 * Create the dialog.
	 */
	public CadEndereco(Endereco endereco) {
		
		setTitle("Cadastro de Endereço");
		setModal(true);
		setResizable(false);
		setBounds(100, 100, 505, 220);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.SOUTH);
		JLabel lblLogradouro = new JLabel("Logradouro");
		
		tfLogradouro = new JTextField();
		tfLogradouro.setColumns(10);
		
		lblNumero = new JLabel("Número");
		
		tfNumero = new JTextField();
		tfNumero.setColumns(10);
		
		lblBairro = new JLabel("Bairro");
		
		tfBairro = new JTextField();
		tfBairro.setColumns(10);
		
		//ftfCep = new JFormattedTextField();
		ftfCep = new JFormattedTextField(SwUtil.criaMascara("#####-###"));
		
		lblCEP = new JLabel("CEP");
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(this);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(lblLogradouro)
									.addGap(267))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(tfBairro, Alignment.LEADING)
										.addComponent(tfLogradouro, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
										.addComponent(btnSalvar))
									.addPreferredGap(ComponentPlacement.UNRELATED)))
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNumero)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
									.addComponent(tfNumero, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblCEP)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
											.addComponent(btnFechar)
											.addComponent(ftfCep, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))))))
						.addComponent(lblBairro))
					.addContainerGap(133, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLogradouro)
						.addComponent(lblNumero))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfNumero, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfLogradouro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBairro)
						.addComponent(lblCEP))
					.addGap(7)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfBairro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(ftfCep, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnFechar))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		
		//Fim do Construtor CadEndereco
		atualizaTela(endereco); 
	}
	
	public void atualizaTela(Endereco endereco) {
		this.endereco = endereco;
		tfLogradouro.setText(endereco.getLogradouro());
		tfNumero.setText(endereco.getNumero());
		tfBairro.setText(endereco.getBairro());
		ftfCep.setValue(endereco.getCep());		
	}
	
	public Endereco getEndereco() {
		return this.endereco;
	}
	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		if(botao.equals(btnSalvar)) {
			if (tfLogradouro.getText().length() == 0)
				JOptionPane.showMessageDialog(this, "Digite o logradouro");
			else if (tfNumero.getText().length() == 0)
				JOptionPane.showMessageDialog(this, "Digite o número");
			else if (tfBairro.getText().length() == 0)
				JOptionPane.showMessageDialog(this, "Digite o bairro");
			else if (ftfCep.getText().trim().length() == 1)
				JOptionPane.showMessageDialog(this, "Digite o cep, " + ftfCep.getText());
			else {
				this.endereco.setLogradouro(tfLogradouro.getText());
				this.endereco.setNumero(tfNumero.getText());
				this.endereco.setBairro(tfBairro.getText());
				this.endereco.setCep(ftfCep.getText());
				this.setVisible(false);
			}
		}
		else {
			this.endereco = null;
	    	this.setVisible(false);
		}
	}
}
