package logica.Aula10.exerciciosGui3.view.adapter;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import logica.Aula10.exerciciosGui3.model.Produto;

public class ProdutoModel extends AbstractTableModel {
    String nomeColunas[] = {"Nome","preço"};
    List<Produto> produtos;
     
    public  ProdutoModel(List<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public int getColumnCount() {
		return nomeColunas.length;
	}

	@Override
	public int getRowCount() {
		return produtos.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		String valor = null;
		Produto produto = produtos.get(linha);
		
		switch(coluna){
		case 0:
			valor = produto.getNome();
			break;
		case 1:
			valor = String.valueOf(produto.getPreco());
			break;
		}
		return valor;
	}

    @Override
	public String getColumnName(int coluna) {
		return nomeColunas[coluna];
	}
    				
	public void adicionar(Produto prod) {
		produtos.add(prod);
		fireTableDataChanged();
	}
	
	public void remover(int linha) {
	    produtos.remove(linha);
	    fireTableDataChanged();
	}
	
	public Produto getProduto(int Linha) {		
		return produtos.get(Linha);		
	}
	
	public void atualizar(int linha, Produto prod) {
	    produtos.set(linha, prod);
	    fireTableDataChanged();
	}
	
	
} 
