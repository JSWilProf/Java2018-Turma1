package logica.Aula10.exerciciosGui3.view;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;

import logica.Aula10.exerciciosGui3.model.Endereco;
import logica.Aula10.exerciciosGui3.model.Fornecedor;
import logica.Aula10.exerciciosGui3.model.Produto;
import logica.Aula10.exerciciosGui3.view.adapter.ProdutoModel;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;

public class CadFornecedor extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JButton btnRegistrarEnd;
	private JLabel lblEnd;
	private JPanel panel;
	private JButton btnSalvar;
	private JButton btnListar;
	private JButton btnSair;
	private JButton btnAdicionarProd;
	private JButton btnRemoverProd;
	private JScrollPane scrollPane;
	private JTable table;

	// Cadastro de Fornecedor Memória
	private List<Fornecedor> cadastro = new ArrayList<>();

	// Lista dos Produtos de UM (1) Fornecedor
	private List<Produto> produtos;

	// Os dados de produtos para o JTable
	private ProdutoModel model;
	
	// Endereco de  unico fornercedor
	private Endereco endereco =new Endereco();
	private JButton btnEditarProd;
	
	public CadFornecedor() {
		setResizable(false);
		setTitle("Cadastro de Fornecedores");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 630, 479);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		btnRegistrarEnd = new JButton("...");
		btnRegistrarEnd.addActionListener(this);
		
		lblEnd = new JLabel("End.");
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Produtos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnListar = new JButton("Listar");
		btnListar.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNome)
								.addComponent(lblEnd))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnRegistrarEnd)
								.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 523, GroupLayout.PREFERRED_SIZE)))
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 591, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnSalvar)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnListar)
							.addPreferredGap(ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
							.addComponent(btnSair)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnRegistrarEnd)
						.addComponent(lblEnd))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 296, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnListar)
						.addComponent(btnSair))
					.addContainerGap(29, Short.MAX_VALUE))
		);
		
		btnAdicionarProd = new JButton("+");
		btnAdicionarProd.addActionListener(this);
		
		btnRemoverProd = new JButton("-");
		btnRemoverProd.addActionListener(this);
		
		scrollPane = new JScrollPane();
		
		btnEditarProd = new JButton("");
		btnEditarProd.addActionListener(this);
		btnEditarProd.setIcon(new ImageIcon("C:\\Users\\javaprg\\Desktop\\workspace\\Logica\\src\\logica\\Aula10\\exerciciosGui3\\view\\lapis.png"));

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnAdicionarProd)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnRemoverProd)
							.addGap(18)
							.addComponent(btnEditarProd, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 557, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(12, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAdicionarProd)
						.addComponent(btnRemoverProd)
						.addComponent(btnEditarProd))
					.addGap(19))
		);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		
		//Fim do Construtor CadFornecedor---------------------------------------------- 
        configuraTabela();
		
		// Define o botÃ£o OK como o botÃ£o padrÃ£o do formulario (Que aceita o ENTER)
		getRootPane().setDefaultButton(btnSalvar);
		// Centraliza a Janela no Centro da tela do computador
		setLocationRelativeTo(null);
	}
	
	public void configuraTabela() {
		produtos = new ArrayList<>();
		model = new ProdutoModel(produtos);
		table.setModel(model);
	}
	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		if (botao.equals(btnRegistrarEnd)) {
			try {
				CadEndereco tela = new CadEndereco(endereco);
				tela.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				tela.setLocationRelativeTo(null);
				tela.setVisible(true);
				
			} catch (Exception e) { 
				e.printStackTrace();
			}	
		}
		else if (botao.equals(btnSair)) {
			System.exit(0);
		}
		else if (botao.equals(btnSalvar)) {
			String nome = tfNome.getText();
			
			Fornecedor forn = new Fornecedor();
			forn.setNome(nome);
			
			
			if (endereco == null) {
				JOptionPane.showMessageDialog(null, "Endereço não registrado.");
			}
			else if (produtos.size() ==0) {
				JOptionPane.showMessageDialog(null, "Produto não registrado.");				
			}
			else {
				forn.setEndereco(endereco);
				forn.setProdutos(produtos);
				
				configuraTabela();
				
				cadastro.add(forn);
				tfNome.setText("");
				endereco = new Endereco();
			}
				

		}
		else if (botao.equals(btnListar)) {
			String msg = "Cadastro de fornecedores\n";
			for(Fornecedor forn: cadastro) {
				msg += forn + "\n";
			}
			JOptionPane.showMessageDialog(null, msg);
		}
		else if (botao.equals(btnEditarProd)) {
			Produto prod = new Produto();
			
			
			int linha = table.getSelectedRow();
			if(linha != -1) {
				if(table.getRowSorter() != null) 
					linha = table.getRowSorter().convertRowIndexToModel(linha);				
				prod = model.getProduto(linha);
				
				CadProduto tela = new CadProduto(prod);
				tela.setLocationRelativeTo(null);
				tela.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				tela.setVisible(true);
				if (tela.getProduto() != null) {
					model.atualizar(linha, prod);
				}		
				
			} else 
				JOptionPane.showMessageDialog(this, "Selecione um Produto");
			
		    
			
			
			
		}	
		else if (botao.equals(btnAdicionarProd)) {
			Produto prod = new Produto();
			
			CadProduto tela = new CadProduto(prod);
			tela.setLocationRelativeTo(null);
			tela.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			tela.setVisible(true);
			
			if (tela.getProduto() != null) {
				model.adicionar(prod);
			}
		}	
		else if (botao.equals(btnRemoverProd)) {
			int linha = table.getSelectedRow();
			if(linha != -1) {
				if(table.getRowSorter() != null) 
					linha = table.getRowSorter().convertRowIndexToModel(linha);				
				
				int op = JOptionPane.showConfirmDialog(this, 
						"Confirma excluir o Produto", "Excluir Produto",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(op == 0)
					model.remover(linha);
			} else 
				JOptionPane.showMessageDialog(this, "Selecione um Produto");			
		}		
	}
}
