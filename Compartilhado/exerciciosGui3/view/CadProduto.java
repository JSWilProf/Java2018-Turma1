package logica.Aula10.exerciciosGui3.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.Aula10.exerciciosGui3.model.Produto;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.event.ActionEvent;

public class CadProduto extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblProduto;
	private JTextField tfProduto;
	private JLabel lblPreco;
	private JTextField tfPreco;
	private JButton btnSalvar;
	private JButton btnFechar;

    private Produto produto;  
	/**
	 * Create the dialog.
	 */
	public CadProduto(Produto produto) {
		setModal(true);
		setResizable(false);
		setTitle("Cadastro do Produto");
		setBounds(100, 100, 527, 149);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		lblProduto = new JLabel("Produto");
		
		tfProduto = new JTextField();
		tfProduto.setColumns(10);
		
		lblPreco = new JLabel("Preço");
		
		tfPreco = new JTextField();
		//tfPreco.addActionListener(this);
		tfPreco.setColumns(10);
		
		tfPreco.addKeyListener(new KeyAdapter() {
			   public void keyTyped(KeyEvent evt){
					Object comp = evt.getSource();
					if (comp instanceof JTextField) {
						JTextField tf = (JTextField) evt.getSource();
						int posicao = tf.getText().indexOf(',');
						int tamanho = tf.getText().length();
						
						char c = evt.getKeyChar();
						if (!(Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)
								|| (c == KeyEvent.VK_COMMA && posicao == -1)   )) {
							evt.consume();
						}
							
						if ( posicao>0 &&(tamanho - posicao) > 2) {
							evt.consume();
						}
							
								
							
						
					}
			   }
			});
			
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(this);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnSalvar)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(tfProduto, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblProduto)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblPreco)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
							.addComponent(btnFechar)
							.addComponent(tfPreco, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblProduto)
						.addComponent(lblPreco))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfProduto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfPreco, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnFechar))
					.addContainerGap(159, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		
	    //Fim do Construtor CadProduto
		atualizaTela(produto);
	}
	
	public void atualizaTela(Produto produto) {
		this.produto = produto;
		tfProduto.setText(produto.getNome());
		double preco = produto.getPreco();
		if (preco > 0) 
		   tfPreco.setText(String.valueOf(preco));
		else	
			tfPreco.setText("");	
	}
	public Produto getProduto() {
		return this.produto;
	}
	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		if (botao.equals(btnSalvar)) {
			
			if (tfProduto.getText().length() ==0) {
				JOptionPane.showMessageDialog(this,"Digite o nome do produto");
			}
			else if (tfPreco.getText().length() == 0){
				JOptionPane.showMessageDialog(this,"Digite o preço do produto");
			}
			else {	
				double preco;
				
				 NumberFormat formatador = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

				 try {
					 preco = formatador.parse(tfPreco.getText()).doubleValue();
					 if (preco == 0) {
						 JOptionPane.showMessageDialog(this,"O preço do produto deve ser maior que zero");
					 }
					 else {
						 produto.setNome(tfProduto.getText());
						 produto.setPreco(preco);
						 setVisible(false);
					 }
				 }
				 catch(ParseException erro){
					 JOptionPane.showMessageDialog(null, "O valor informado estÃ¡ incorreto!\n" +
							 erro.getMessage());
					 //erro.printStackTrace();
				 }
			       
			}
		}
		else {
			produto = null;
			setVisible(false);
		}
		
	}
}
