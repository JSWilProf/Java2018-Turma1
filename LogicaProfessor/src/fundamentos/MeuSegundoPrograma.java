package fundamentos;

import javax.swing.JOptionPane;

import fundamentos.libOld.Utilitarios;

public class MeuSegundoPrograma {
	public static void main(String[] args) {
		int qtd = Utilitarios.leInteiro("Informe a quantidade");
		
		double valor = Utilitarios.leReal("Informe o Valor");
		
		double total = qtd * valor;
		
		JOptionPane.showMessageDialog(null, "O Resultado é R$ " + total);
	}
}
