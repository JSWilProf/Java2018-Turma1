package fundamentos.resposta;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.xml.ws.Holder;

public class Ex02 {
	public static void main(String[] args) {
		NumberFormat formatador = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

		String msg = "Cálculo do Salário\n\n";

		try {
			String temp = JOptionPane.showInputDialog("Informe a Qtd de horas trabalhadas");
			int horas = Integer.parseInt(temp);
			msg += "Horas Trabalhadas: " + horas + "\n";
			
			temp = JOptionPane.showInputDialog("Informe o Salário Hora");
			double salHora = formatador.parse(temp).doubleValue();
			msg += String.format("Salário Hora: R$ %,.2f\n", salHora);

			temp = JOptionPane.showInputDialog("Informe o nº de dependentes");
			int dep = Integer.parseInt(temp);
			msg += "Nº de dependentes: " + dep + "\n";
			
			double salBruto = horas * salHora + 50 * dep;
			msg += String.format("Salário Bruto: R$ %,.2f\n", salBruto);

			double inss = 0;
			if(salBruto <= 1000) {
				inss = salBruto * 8.5 / 100;
			} else {
				inss = salBruto * 9 / 100;
			}
			msg += String.format("INSS: R$ %,.2f\n", inss);
			

			double ir = 0;
			if(salBruto <= 500) {
				ir = 0;
			} else if(salBruto <= 1000) {
				ir = salBruto * 5 / 100;
			} else {
				ir = salBruto * 7 / 100;
			}
			msg += String.format("IR: R$ %,.2f\n", ir);
			
			double salLiq = salBruto - inss - ir;
			msg += String.format("Salário Líquido: R$ %,.2f\n", salLiq);
			
			JOptionPane.showMessageDialog(null, msg);
		} catch (ParseException erro) {
			JOptionPane.showMessageDialog(null, "O valor informado está incorreto!\n" +
							erro.getMessage());
		}
	}
}
