package fundamentos.resposta;

import javax.swing.JOptionPane;

public class Ex01 {
	public static void main(String[] args) {
		String msg = "Resumo do Pedido\n\n";
		
		String temp = JOptionPane.showInputDialog("Informe a 1ª Qtd");
		int qtd1 = Integer.parseInt(temp);
		msg += "Quantidade: " + qtd1 + "\n";
		temp = JOptionPane.showInputDialog("Informe o 1º Valor");
		double val1 = Double.parseDouble(temp);
		msg += String.format("Valor: R$ %,.2f\n", val1);
		
		temp = JOptionPane.showInputDialog("Informe a 2ª Qtd");
		int qtd2 = Integer.parseInt(temp);
		msg += "Quantidade: " + qtd2 + "\n";
		temp = JOptionPane.showInputDialog("Informe o 2º Valor");
		double val2 = Double.parseDouble(temp);
		msg += String.format("Valor: R$ %,.2f\n", val2);
		
		temp = JOptionPane.showInputDialog("Informe a 3ª Qtd");
		int qtd3 = Integer.parseInt(temp);
		msg += "Quantidade: " + qtd3 + "\n";
		temp = JOptionPane.showInputDialog("Informe o 3º Valor");
		double val3 = Double.parseDouble(temp);
		msg += String.format("Valor: R$ %,.2f\n", val3);
		
		double total = qtd1 * val1 + qtd2 * val2 + qtd3 * val3;
		msg += String.format("O Total: R$ %,.2f\n", total);
		
		JOptionPane.showMessageDialog(null, msg);	
	}
}
