package fundamentos.resposta;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JOptionPane;

public class Ex01b {
	public static void main(String[] args) {
		NumberFormat formatador = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

		String msg = "Resumo do Pedido\n\n";
		double total = 0;
		try {
			for (int i = 0; i < 3; i++) {
				String temp = JOptionPane.showInputDialog("Informe a "+(i+1)+"ª Qtd");
				int qtd = Integer.parseInt(temp);
				msg += "Quantidade: " + qtd + "\n";
				temp = JOptionPane.showInputDialog("Informe o "+(i+1)+"º Valor");
				double val = formatador.parse(temp).doubleValue();
				msg += String.format("Valor: R$ %,.2f\n", val);
				total += qtd * val;
			}

			msg += String.format("O Total: R$ %,.2f\n", total);

			JOptionPane.showMessageDialog(null, msg);
		} catch (ParseException erro) {
			JOptionPane.showMessageDialog(null, "O valor informado está incorreto!\n" +
							erro.getMessage());
			//erro.printStackTrace();
		}
	}
}
