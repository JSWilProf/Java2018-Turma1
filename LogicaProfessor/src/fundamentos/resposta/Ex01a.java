package fundamentos.resposta;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JOptionPane;

public class Ex01a {
	public static void main(String[] args) {
		NumberFormat formatador = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

		String msg = "Resumo do Pedido\n\n";

		try {
			String temp = JOptionPane.showInputDialog("Informe a 1ª Qtd");
			int qtd1 = Integer.parseInt(temp);
			msg += "Quantidade: " + qtd1 + "\n";
			temp = JOptionPane.showInputDialog("Informe o 1º Valor");
			double val1 = formatador.parse(temp).doubleValue();
			msg += String.format("Valor: R$ %,.2f\n", val1);

			temp = JOptionPane.showInputDialog("Informe a 2ª Qtd");
			int qtd2 = Integer.parseInt(temp);
			msg += "Quantidade: " + qtd2 + "\n";
			temp = JOptionPane.showInputDialog("Informe o 2º Valor");
			double val2 = formatador.parse(temp).doubleValue();
			msg += String.format("Valor: R$ %,.2f\n", val2);

			temp = JOptionPane.showInputDialog("Informe a 3ª Qtd");
			int qtd3 = Integer.parseInt(temp);
			msg += "Quantidade: " + qtd3 + "\n";
			temp = JOptionPane.showInputDialog("Informe o 3º Valor");
			double val3 = formatador.parse(temp).doubleValue();
			msg += String.format("Valor: R$ %,.2f\n", val3);

			double total = qtd1 * val1 + qtd2 * val2 + qtd3 * val3;
			msg += String.format("O Total: R$ %,.2f\n", total);

			JOptionPane.showMessageDialog(null, msg);
		} catch (ParseException erro) {
			JOptionPane.showMessageDialog(null, "O valor informado está incorreto!\n" +
							erro.getMessage());
			//erro.printStackTrace();
		}
	}
}
