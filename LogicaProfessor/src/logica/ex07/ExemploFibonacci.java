package logica.ex07;

import br.senai.sp.info132.console.Programa;

public class ExemploFibonacci extends Programa {
	@Override
	public void inicio() {
		while(true) {
			int num = leInteiro("Informe o nº de semanas");
			escrevaL("O nº de casais é: ", fib(num));
		}
	}
	
	//fib(n) => fib(n - 2) + fib(n - 1) para n > 2
	public int fib(int n) {
		int casais = 1;
		if(n > 2)
			casais = fib(n - 2) + fib(n - 1);
		return casais;

	}
}
