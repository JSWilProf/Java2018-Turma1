package logica.ex07;

import br.senai.sp.info132.console.Programa;

public class ExemploHanoi extends Programa {
	public void inicio() {
		int num = leInteiro("Informe o nº de discos");
		escrevaL("O nº de passos é: ", hanoi(num));
	}
	
	// hano(n) => 2 * hanoi(n - 1) + 1 para n > 1
	public int hanoi(int n) {
		int passos = 1;
		if(n > 1)
			passos = 2 * hanoi(n - 1) + 1;
		return passos;
	}
}
