package logica.ex07.respostas;

import br.senai.sp.info132.console.Programa;

public class Ex03 extends Programa {
	@Override
	public void inicio() {
		int conta = leInteiro("Informe o nº da conta");
		escrevaL("O dígito verificador para a conta ", conta, " é ", digito(conta));
	}
	
	public int digito(int num) {
		int soma = num + Ex02.inverso(num);
		int dig = 0;
		for (int i = 6; i > 0; i--) {
			dig += soma % 10 * i;
			soma /= 10;
		}
		
		return dig % 10;
	}
}
