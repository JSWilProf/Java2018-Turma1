package logica.ex07.respostas;

import br.senai.sp.info132.console.Programa;

public class Ex05 extends Programa {
	@Override
	public void inicio() {
		int num = leInteiro("Informe o nº para o calculo do Fatorial");
		escrevaL("O valor calculado com a função fatorial para ", num,
				" é ", fat(num));
	}
	
	// fat(n) => n * fat(n - 1) enquanto n > 1
	public int fat(int n) {
		int f = 1;
		if(n > 1) {
//			escrevaL("f = ", n , " * fat(", n, " - 1)");
			f = n * fat(n - 1);
		}
//		escrevaL("fat(", n, ") => ", f);
		return f;
	}
}
