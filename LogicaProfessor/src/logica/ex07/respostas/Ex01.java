package logica.ex07.respostas;

import br.senai.sp.info132.console.Programa;

public class Ex01 extends Programa {
	@Override
	public void inicio() {
		int num = leInteiro("Informe o nº");
		escrevaL("O nº ", num, " é ", ePar(num) ? "par" : "impar");
	}
	
	public boolean ePar(int num) {
		return num % 2 == 0;
	}
}
