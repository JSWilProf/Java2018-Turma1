package logica.lib;

import java.lang.reflect.Array;

public class Utilitarios {
	public static void ordena(int[] valor) {
		boolean trocou;
		do {
			trocou = false;
			for (int i = 0; i < valor.length - 1; i++) {
				if (valor[i] > valor[i + 1]) {
					int aux = valor[i + 1];
					valor[i + 1] = valor[i];
					valor[i] = aux;
					trocou = true;
				}
			}
		} while (trocou);

	}

	public static <Tipo> Tipo[] aumentaTamanho(Tipo[] vetor) {
		return aumentaTamanho(vetor, 5);
	}

	// TODO: Apresentar a solução do Método Genérico
	@SuppressWarnings("unchecked")
	public static <Tipo> Tipo[] aumentaTamanho(Tipo[] vetor, int incremento) {
		Class<Tipo> tipo = (Class<Tipo>) vetor[0].getClass();
		Tipo[] novo = (Tipo[]) Array.newInstance(tipo, vetor.length + incremento);

		for (int i = 0; i < vetor.length; i++) {
			novo[i] = vetor[i];
		}

		return novo;
	}
}
