package logica.ex06.respostas;

import br.senai.sp.info132.console.Programa;
import logica.lib.Utilitarios;

public class Ex01 extends Programa {
	@Override
	public void inicio() {
		int[] valor = new int[10];
		int media = 0;
		
		for (int pos = 0; pos < valor.length; pos++) {
			valor[pos] = leInteiro("Informe o " , pos+1, "º valor");
			media += valor[pos];
		}
		
		media /= valor.length;
		escrevaL("Média: ", media);
		
		Utilitarios.ordena(valor);
		
		for (int pos = 0; pos < valor.length; pos++) {
			if(valor[pos] >= media) {
				escrevaL(valor[pos]);
			}
		}
	}
}
