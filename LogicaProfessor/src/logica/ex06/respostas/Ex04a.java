package logica.ex06.respostas;

import java.util.Arrays;

import br.senai.sp.info132.console.Programa;

public class Ex04a extends Programa {
	public void inicio() {
		Funcionario[] cadastro = new Funcionario[5];
		int idx = 0;
		
		String nome = leTexto("Informe o nome ou \"fim\" para encerrar");
		while(!nome.equals("fim")) {
			Funcionario func = new Funcionario();
			func.setNome(nome);
			func.setSalario(leReal("Informe o salário"));
			cadastro[idx] = func;
			
			idx++;
			
			if(idx >= cadastro.length) {
				cadastro = Arrays.copyOf(cadastro, cadastro.length + 5);
			}
			
			nome = leTexto("Informe o nome ou \"fim\" para encerrar");
		}
		
		ordena(cadastro, idx);
		
		escrevaL("\nFuncionário\t\tSalário");
		escrevaL("------------\t\t----------");
		for (int i = 0; i < cadastro.length; i++) {
			if(cadastro[i] != null)
				escrevaL(cadastro[i].toString());
		}
	}

	public void ordena(Funcionario[] cad, int idx) {
		boolean trocou;
		do {
			trocou = false;
			for (int i = 0; i < idx - 1; i++) {
				if(cad[i].getSalario() < cad[i+1].getSalario()) {
					
					Funcionario aux = cad[i+1];
					cad[i+1] = cad[i];
					cad[i] = aux;
					
					trocou = true;
				}
			}
		} while(trocou);

	}
	
}
