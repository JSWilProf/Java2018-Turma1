package logica.ex06.respostas;

public class Funcionario /*implements Comparable<Funcionario> */{
	private String nome;
	private double salario;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return nome + "\t\t\t" + salario;
	}

//	@Override
//	public int compareTo(Funcionario outro) {
//		if(salario >  outro.salario) {
//			return 1;
//		} else if(salario <  outro.salario) {
//			return -1;
//		} else {
//			return 0;
//		}
//	}
	
}
