package logica.ex06.respostas;

import br.senai.sp.info132.console.Programa;

public class Ex02a extends Programa {
	@Override
	public void inicio() {
		String[] nomes = new String[10];
		
		for (int i = 0; i < nomes.length; i++) {
			nomes[i] = leTexto("Informe o ", i+1 ,"º nome");
		}
		
		String pesquisa = leTexto("Informe um Nome");
		
		while(!pesquisa.equals("fim")) {	
			boolean encontrou = false;
			
			for (int i = 0; i < nomes.length; i++) {
				if(pesquisa.equals(nomes[i])) {
					encontrou = true;
				}
			}
			
			if(encontrou) {
				escrevaL("O nome ", pesquisa, " foi encontrado");
			} else {
				escrevaL("O nome ", pesquisa, " não foi encontrado");
			}
			
			pesquisa = leTexto("Informe um Nome");
		}	
		
	}

}
