package logica.ex06.respostas;

import br.senai.sp.info132.console.Programa;
import logica.lib.Utilitarios;

public class TesteOrdenacao extends Programa {
	@Override
	public void inicio() {
		int[] valor = {
				23,45,76,12,67,34,89,23,16,41,46,8,23,8,1,78 
		};
		
		Utilitarios.ordena(valor);
		
		for (int i = 0; i < valor.length; i++) {
			escreva(valor[i], " ");
		}
	}
}
