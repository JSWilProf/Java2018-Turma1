package logica.ex06.respostas;

import java.util.Arrays;

import br.senai.sp.info132.console.Programa;

public class Ex04 extends Programa {
	public void inicio() {
		String[] nomeFunc = new String[1];
		double[] salario = new double[1];
		
		int idx = 0;
		
		String nome = leTexto("Informe o nome ou \"fim\" para encerrar");
		while(!nome.equals("fim")) {
			nomeFunc[idx] = nome;
			salario[idx] = leReal("Informe o salário");
			
			idx++;
			
			if(idx >= nomeFunc.length) {
				nomeFunc = Arrays.copyOf(nomeFunc, nomeFunc.length + 5);
				salario = Arrays.copyOf(salario, salario.length + 5);
			}
			
			nome = leTexto("Informe o nome ou \"fim\" para encerrar");
		}
		
		ordena(nomeFunc, salario);
		
		escrevaL("\nFuncionário\t\tSalário");
		escrevaL("------------\t\t----------");
		for (int i = 0; i < nomeFunc.length; i++) {
			if(nomeFunc[i] != null)
				escrevaL(nomeFunc[i], "\t\t\t", salario[i]);
		}
	}

	public void ordena(String[] nome, double[] sal) {
		boolean trocou;
		do {
			trocou = false;
			for (int i = 0; i < sal.length - 1; i++) {
				if(sal[i] < sal[i+1]) {
					
					double aux = sal[i+1];
					sal[i+1] = sal[i];
					sal[i] = aux;
					
					String nom = nome[i+1];
					nome[i+1] = nome[i];
					nome[i] = nom;
					
					trocou = true;
				}
			}
		} while(trocou);

	}
	
}
