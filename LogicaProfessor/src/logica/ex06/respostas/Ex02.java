package logica.ex06.respostas;

import br.senai.sp.info132.console.Programa;

public class Ex02 extends Programa {
	@Override
	public void inicio() {
		String[] nomes = new String[10];
		
		coleteNomes(nomes);
		
		String pesquisa = leTexto("Informe um Nome");
		
		while(!pesquisa.equals("fim")) {		
			if(consulteNome(nomes, pesquisa)) {
				escrevaL("O nome ", pesquisa, " foi encontrado");
			} else {
				escrevaL("O nome ", pesquisa, " não foi encontrado");
			}
			
			pesquisa = leTexto("Informe um Nome");
		}	
		
	}

	private boolean consulteNome(String[] nomes, String pesquisa) {
		for (int i = 0; i < nomes.length; i++) {
			if(pesquisa.equals(nomes[i])) {
				return true;
			}
		}
		
		return false;
	}

	private void coleteNomes(String[] nomes) {
		for (int i = 0; i < nomes.length; i++) {
			nomes[i] = leTexto("Informe o ", i+1 ,"º nome");
		}
	}
}
