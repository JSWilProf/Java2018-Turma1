package logica.ex06.respostas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.senai.sp.info132.console.Programa;

public class Ex04b extends Programa {
	public void inicio() {
		List<Funcionario> cadastro = new ArrayList<>();

		String nome = leTexto("Informe o nome ou \"fim\" para encerrar");
		while (!nome.equals("fim")) {
			Funcionario func = new Funcionario();
			func.setNome(nome);
			func.setSalario(leReal("Informe o salário"));
			cadastro.add(func);

			nome = leTexto("Informe o nome ou \"fim\" para encerrar");
		}

		Collections.sort(cadastro, 
				Comparator.comparing(Funcionario::getSalario).reversed());
//		Collections.sort(cadastro, new Comparator<Funcionario>() {
//			public int compare(Funcionario func1, Funcionario func2) {
//				if (func1.getSalario() > func2.getSalario()) {
//					return -1;
//				} else if (func1.getSalario() < func2.getSalario()) {
//					return 1;
//				} else {
//					return 0;
//				}
//			}
//		});

		escrevaL("\nFuncionário\t\tSalário");
		escrevaL("------------\t\t----------");
		for (Funcionario func : cadastro) {
			escrevaL(func.toString());
		}
//		for (int i = 0; i < cadastro.size(); i++) {
//			escrevaL(cadastro.get(i).toString());
//		}
	}

}

 
