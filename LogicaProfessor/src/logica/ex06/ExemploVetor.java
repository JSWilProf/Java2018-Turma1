package logica.ex06;

import br.senai.sp.info132.console.Programa;

public class ExemploVetor extends Programa {

public void inicio() {

	
/*	
	int[][] contador = new int[3][]; 
	contador[0] = new int[3];
	contador[1] = new int[2];
	contador[2] = new int[5];	 
     0  1  2  3  4
0	[ ][ ][ ]
1	[ ][ ]
2	[ ][ ][8][ ][ ]
	
    int mes = 2;
    int vendedor = 2;
 
    contador[mes][vendedor] = 8;
 
 int[][] contador = new int[5][3]; 
     0  1  2
0	[ ][ ][ ]
1	[ ][ ][ ]
2	[ ][9][ ]  contador[2][1] = 9;
3	[ ][ ][ ]
4	[ ][ ][ ]


 int[] contador = new int[5]; 	 
     0  1  2  3  4
 	[ ][ ][ ][ ][ ]

	
	
	String[] nomes = new int[5]; 
                             ^
                             +- - nomes.length
        0  1  2  3  4    
       [ ][ ][ ][ ][ ]
	    ^
	    |
	int i = 0;
	while(i < nomes.length) {
		nomes[i] = leTexto("Informe o nome do ", i+1 ,"º vendedor");
		i++;
	}

	int i = 0;
	while(i < nomes.length) {
		nomes[i++] = leTexto("Informe o nome do ", i+1 ,"º vendedor");
	}


	for(int i = 0;i < nomes.length) {
		nomes[i++] = leTexto("Informe o nome do ", i+1 ,"º vendedor");
	}


	for(int i = 0;i < nomes.length;i++) {
		nomes[i] = leTexto("Informe o nome do ", i+1 ,"º vendedor");
	}





 int[][] vendas = new int[5][3]; 
     0  1  2
0	[ ][ ][ ]
1	[ ][ ][ ]
2	[ ][ ][ ]  
3	[ ][ ][ ]
4	[ ][ ][ ]




*/
	 String[] vendedores = { "João", "José", "Maria" };
	 String[] meses = { "Janeiro", "Fevereiro"};
	 double[][] vendas = new double[2][3]; 
	 
	 for (int mes = 0; mes < vendas.length; mes++) {
		 escrevaL("Vendas no mês ", meses[mes]);
		for (int vendedor = 0; vendedor < vendas[mes].length; vendedor++) {
			vendas[mes][vendedor] = leReal("Informe o valor para o vendedor ", vendedores[vendedor]);
		}
	 }

	 escrevaL("------------------------");
	 
	 for (int mes = 0; mes < vendas.length; mes++) {
		escrevaL("O Valor de Vendas no mês ", meses[mes]);
		for (int vendedor = 0; vendedor < vendas[mes].length; vendedor++) {
			escrevaL(vendedores[vendedor] ,"º vendedor : R$ ", vendas[mes][vendedor]);
		}
	 }
	 
  }
}












