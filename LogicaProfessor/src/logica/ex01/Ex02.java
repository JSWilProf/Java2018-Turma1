package logica.ex01;

import br.senai.sp.info132.console.Programa;

public class Ex02 extends Programa {
	public void inicio() {
		double nota1 = leReal("Informe a 1ª nota");
		double nota2 = leReal("Informe a 2ª nota");
		double nota3 = leReal("Informe a 3ª nota");
		double nota4 = leReal("Informe a 4ª nota");
		
		double media = (nota1 + nota2 + nota3 + nota4) / 4;
		
		escrevaL("A média é de: ", media);
	}
}
