package logica.ex01;

import br.senai.sp.info132.console.Programa;

public class Ex03 extends Programa {
	public void inicio() {
		double base = leReal("Informe a Base");
		double altura = leReal("Informe a Altura");
		double area = base * altura / 2;
		escrevaL("A Área é de: ", area);
	}
}
