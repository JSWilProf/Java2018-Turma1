package logica.ex01;

import br.senai.sp.info132.console.Programa;

public class ExemploUnario extends Programa {
	@Override
	public void inicio() {
		int num = -1;
		escreva("num: ", -num);
	}
}
