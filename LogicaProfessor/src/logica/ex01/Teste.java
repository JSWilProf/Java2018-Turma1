package logica.ex01;

import br.senai.sp.info132.console.Programa;

public class Teste extends Programa {

	@Override
	public void inicio() {
		String nome = leTexto("Informe seu Nome");
		escreva("Bem vindo, ", nome);		
	}
}
