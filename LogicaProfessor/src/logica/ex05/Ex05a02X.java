package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a02X extends Programa {
	@Override                       
	public void inicio() {  
		Profissional[] funcoes = {
			new Profissional("analista"),
			new Profissional("programador"),
			new Profissional("usuario"),
			new Profissional("gestor"),
			new Profissional("outro"),
		};
		
		String funcao = leTexto("Informe a Função");    // analista

		while (!funcao.equals("fim")) {
			for(int posicao = 0;posicao < funcoes.length;posicao++) {
				if (funcao.toLowerCase().equals(funcoes[posicao].nomeDaFuncao)) {  // nomeDaFuncao[0] ==> analista
					funcoes[posicao].contador++;
				}
			}

			funcao = leTexto("Informe a Função");
		}
		
		String msg = "";
		for (int posicao = 0; posicao < funcoes.length; posicao++) {
			msg += funcoes[posicao] + "\n";
		}
		
		escrevaL(msg);
	}
}

class Profissional {
	public String nomeDaFuncao;
	public int contador;
	
	public Profissional(String nome) {
		nomeDaFuncao = nome;
		contador = 0;
	}
	
	public String toString() {
		return nomeDaFuncao + ": " + contador;
	}
}