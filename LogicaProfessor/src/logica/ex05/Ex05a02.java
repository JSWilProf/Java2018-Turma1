package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a02 extends Programa {
	@Override
	public void inicio() {
		int analista = 0, programador = 0, usuario = 0, gestor = 0, outro = 0;
		String funcao = leTexto("Informe a Função");

		while (!funcao.equals("fim")) {
			switch (funcao) {
				case "analista": analista++; break;
				case "programador": programador++; break;
				case "usuario": usuario++; break;
				case "gestor": gestor++; break;
				default: outro++; break;
			}
			
			funcao = leTexto("Informe a Função");
		}
		escrevaL("Analista: ", analista, "\nProgramador: ", programador,
				"\nUsuário: ", usuario, "\nGestor: ", gestor, "\nOutro: ", outro );
	}
}
