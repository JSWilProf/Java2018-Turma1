package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a extends Programa {
	@Override
	public void inicio() {
		double salario = 1;
		
		while(salario > 0) {
			salario = leReal("Informe o Salário");

			if(salario < 0) {
				escrevaL("O Salário inválido!");
				salario = 1;
				continue;
			} else if(salario == 0) {
				break;
			} else if(salario <= 500) {
				salario *= 1.2;
			} else {
				salario *= 1.1;
			}
				
			escrevaL("O Salário com aumento é de R$: ", salario);
		}
	}
}
