package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex01a extends Programa {
	@Override
	public void inicio() {
		for (int col = 0, i = 2; i <= 1000; i += 2, col++) {	
			if (col >= 22) {
				col = 0;
				escrevaL();
			}
			escreva(String.format("%04d ", i));
		}
	}
}
