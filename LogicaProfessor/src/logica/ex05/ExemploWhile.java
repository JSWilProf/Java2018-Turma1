package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class ExemploWhile extends Programa {
	@Override
	public void inicio() {
		int contador = 0;
		int quantidade = 2;
		while(contador >= quantidade) {
			escrevaL("funciona....");
		}
		escrevaL("fim.");
	}
}
