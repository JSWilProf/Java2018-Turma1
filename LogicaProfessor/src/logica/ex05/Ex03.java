package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex03 extends Programa {
	@Override
	public void inicio() {
		int total = 0;
		
		for (int i = 0; i < 5; i++) {
			total += leInteiro("Informe o ",i,"º nº");
		}
		
		escrevaL("A soma: ", total);
		escrevaL("A média: ", total/5);
	}
}
