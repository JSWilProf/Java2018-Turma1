package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a03 extends Programa {
	@Override
	public void inicio() {
		int idade = leInteiro("Informe a idade"); 
		int oMaisIdoso = idade;  
		
		while(idade > 0) {  
			if(idade > oMaisIdoso) { 
				oMaisIdoso = idade;
			}
			
			idade = leInteiro("Informe a idade");
		}
		
		escrevaL("O ganhador é a(s) pessoa(s) com: ", oMaisIdoso);
	}
}
