package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex01b extends Programa {
	@Override
	public void inicio() {
		int col = 0, i = 2;
		do {
			if (col >= 22) {
				col = 0;
				escrevaL();
			}
			escreva(String.format("%04d ", i));

			i += 2;
			col++;
		} while (i <= 1000);
	}
}
