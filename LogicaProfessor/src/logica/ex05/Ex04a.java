package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex04a extends Programa {
	@Override
	public void inicio() {
		int quantidade = leInteiro("Informe a quantidade");

		if (quantidade > 0) {
			int oMaior = leInteiro("Informe um nº");

			for (int i = 1; i < quantidade; i++) {
				int num = leInteiro("Informe um nº");

				if (num > oMaior) {
					oMaior = num;
				}
			}

			escrevaL("O maior: ", oMaior);
		} else {
			escrevaL("O maior não foi definido");
		}
	}
}
