package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex01 extends Programa {
	@Override
	public void inicio() {
		for (int i = 2; i < 1000; i+= 2) {
			escreva(String.format("%04d ", i));
		}
	}
}
