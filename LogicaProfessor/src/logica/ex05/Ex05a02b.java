package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a02b extends Programa {
	@Override
	public void inicio() {
		final int ANALISTA = 0;
		final int PROGRAMADOR = 1;
		final int USUARIO = 2;
		final int GESTOR = 3;
		final int OUTRO = 4;
		int[] contador = new int[5];
		
		String funcao = leTexto("Informe a Função");

		while (!funcao.equals("fim")) {
			if (funcao.equals("analista")) {
				contador[ANALISTA]++;
			} else if (funcao.equals("programador")) {
				contador[PROGRAMADOR]++;
			} else if (funcao.equals("usuario")) {
				contador[USUARIO]++;
			} else if (funcao.equals("gestor")) {
				contador[GESTOR]++;
			} else {
				contador[OUTRO]++;
			}
			
			funcao = leTexto("Informe a Função");
		}
		escrevaL("Analista: ", contador[ANALISTA], "\nProgramador: ", contador[PROGRAMADOR],
				"\nUsuário: ", contador[USUARIO], "\nGestor: ", contador[GESTOR], 
				"\nOutro: ", contador[OUTRO] );
	}
}
