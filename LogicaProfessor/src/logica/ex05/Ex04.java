package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex04 extends Programa {
	@Override
	public void inicio() {
		int quantidade = leInteiro("Informe a quantidade");

		int oMaior = Integer.MIN_VALUE;

		for (int i = 0; i < quantidade; i++) {
			int num = leInteiro("Informe um nº");
			
			if(num > oMaior) {
				oMaior = num;
			}
		}
		
		if(oMaior != Integer.MIN_VALUE)
			escrevaL("O maior: ", oMaior);
		else 
			escrevaL("O maior não foi definido");
	}
}
