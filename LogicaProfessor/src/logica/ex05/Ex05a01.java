package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a01 extends Programa {
	@Override
	public void inicio() {
		int qtd = 0;
		double total = 0;
		double valor = leReal("Informe o valor do Produto");
		
		while(valor > 0) {
			total += valor; // total = total + valor;
			qtd++;          // qtd = qtd + 1;
			
			valor = leReal("Informe o valor do Produto");
		}
		escrevaL("A média dos valores dos produtos é de " +
				(total != 0 ? String.format("R$ %,.2f", total / qtd) : "indeterminado") );
	}
}
