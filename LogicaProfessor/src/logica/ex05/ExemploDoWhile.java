package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class ExemploDoWhile extends Programa {
	@Override
	public void inicio() {
		int contador = 0;
		int quantidade = 2;
		do {
			escrevaL("funciona....");
		} while(contador >= quantidade);
		escrevaL("fim.");
	}
}
