package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex05a02C extends Programa {
	@Override                       
	public void inicio() {    //   i >>=     0              1            2        3         4
		String[] nomeDaFuncao =         {"analista", "programador", "usuario", "gestor", "outro"};
		int[] contador = new int[5]; // {    0     ,        0     ,      0   ,    0    ,    0    }
		
		String funcao = leTexto("Informe a Função");    // analista

		while (!funcao.equals("fim")) {
			for(int posicao = 0;posicao < nomeDaFuncao.length;posicao++) {
				if (funcao.toLowerCase().equals(nomeDaFuncao[posicao])) {  // nomeDaFuncao[0] ==> analista
					contador[posicao]++;
				}
			}

			funcao = leTexto("Informe a Função");
		}
		
		String msg = "";
		for (int posicao = 0; posicao < nomeDaFuncao.length; posicao++) {
			msg += nomeDaFuncao[posicao] + ": " + contador[posicao] + "\n";
		}
		
		escrevaL(msg);
	}
}
