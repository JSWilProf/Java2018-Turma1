package logica.ex03;

import br.senai.sp.info132.console.Programa;

public class Ex01c extends Programa {
	@Override
	public void inicio() {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		
		String msg = "São ";
		
//		if(num1 != num2) {
//			msg += "diferentes";
//		} else {
//			msg += "iguais";
//		}
	
		msg += num1 != num2 ? "diferentes" : "iguais";
		
		escrevaL(msg);
	}
}
