package logica.ex03;

import br.senai.sp.info132.console.Programa;

public class Ex04 extends Programa {
	@Override
	public void inicio() {
		double salario = leReal("Informe o Salário");
		
		double salLiq;
		
		if(salario < 300) {
			salLiq = salario - salario * 5 / 100;
		} else if(salario <= 1200) {
			salLiq = salario - salario * 0.1;
		} else {
			salLiq = salario * .85;
		}
		
		escrevaL("O salário líquido é de R$", salLiq);
	}
}
