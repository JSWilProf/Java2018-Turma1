package logica.ex03;

import br.senai.sp.info132.console.Programa;

public class Ex02 extends Programa {
	@Override
	public void inicio() {
		int num = leInteiro("Informe o nº");
				
		if(num % 2 == 0) {
			escrevaL("é Par");
		} else {
			escrevaL("é Impar");
		}
	}
}
