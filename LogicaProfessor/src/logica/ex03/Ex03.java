package logica.ex03;

import br.senai.sp.info132.console.Programa;

public class Ex03 extends Programa {
	@Override
	public void inicio() {
		int A = leInteiro("Informe o nº A");
		int B = leInteiro("Informe o nº B");
		
		if(A % B != 0)
			escreva("não ");
		
		escrevaL("é Divisível");
	}
}
