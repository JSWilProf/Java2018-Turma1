package logica.ex04;

import javax.swing.JOptionPane;

public class Ex04YYY {
	public static void main(String[] args) {
		String nome = JOptionPane.showInputDialog("Informer seu Nome");
				
		int nota1 = leInteiro("Informe a 1ª nota");
		int nota2 = leInteiro("Informe a 2ª nota");
		int nota3 = leInteiro("Informe a 3ª nota");
		double media = (nota1 + nota2 + nota3) / 3;
		
		int faltas = leInteiro("Informe o nº de faltas");
		
		String msg = nome + ", você foi: ";
		
		if(faltas > 15) {
			msg += "Reprovado por Faltas";
		} else if(media < 7) {
			msg += "Reprovado por Média";
		} else {
			msg += "Aprovado";
		}
		
		JOptionPane.showMessageDialog(null, msg);
	}

	/** Este método recebe uma String para apresentar a
	   mensagem que solicita o valor a ser informado,
	   cria uma caixa de mensagem e converte o seu
	   retorno o numero inteiro.
	   
	   @param etiqueta Representa a informação a ser 
	   				  apresentada na caixa de mensagem.
	   				  
	   @return um inteiro representando o valor informado
	   
	   @since 03/02/2018 - Prof
	*/ 
	public static int leInteiro(String etiqueta) {
		String temp = JOptionPane.showInputDialog(etiqueta);
		int nota = Integer.parseInt(temp);
		return nota;
	}
}
