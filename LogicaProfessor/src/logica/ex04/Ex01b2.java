package logica.ex04;

import br.senai.sp.info132.console.Programa;

public class Ex01b2 extends Programa {
	@Override
	public void inicio() {
		double media = 0;
		
		int nota = 1;
		while (nota <= 4) {
			media += leInteiro("Informe a ",nota , "ª nota");
			
			nota++;
		}
		
		media /= 4;
		
		escreva("Média: ", media, " ");
		
		if(media >= 7) {
			escrevaL("Aprovado");
		} else {
			escrevaL("Reprovado");
		}
		
		
	}
}
