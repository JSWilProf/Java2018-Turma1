package logica.ex04;

import br.senai.sp.info132.console.Programa;

public class Ex05 extends Programa {
	@Override
	public void inicio() {
		int horas = leInteiro("Informe as Horas Trabalhadas no mês");
		double salHora = leReal("Informe o Salário hora");
		int dep = leInteiro("Informe o nº de Dependentes");
		
		double salBruto = horas * salHora + 50 * dep;
		escrevaL("Salário Bruto R$ ", salBruto);
		
		double inss;
		if(salBruto <= 1000) {
			inss = salBruto * 0.085;
		} else {
			inss = salBruto * 0.09;
		}
		escrevaL("INSS R$ ", inss);
		
		double ir;
		if(salBruto <= 500) {
			ir = 0;
		} else if(salBruto <= 1000) {
			ir = salBruto * 0.05;
		} else {
			ir = salBruto * 0.07; 
		}
		escrevaL("IR R$ ", ir);
		
		double salLig = salBruto - inss - ir;
		escrevaL("Salário Liquido R$ ", salLig);
		
		
	}
}
