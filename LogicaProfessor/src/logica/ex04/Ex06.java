package logica.ex04;

import br.senai.sp.info132.console.Programa;

public class Ex06 extends Programa {
	@Override
	public void inicio() {
		int contador = 0;
		while (contador < 5) {
			int nota1 = leInteiro("Informe a 1º nota");
			int nota2 = leInteiro("Informe a 2º nota");

			double media = (nota1 + nota2) / 2;

			if (media <= 60) {
				escrevaL("Insuficiente");
			} else if (media <= 80) {
				escrevaL("Satisfatória");
			} else if (media <= 90) {
				escrevaL("Boa");
			} else {
				escrevaL("Excelente");
			}
			
			contador++;
		}
	}
}
