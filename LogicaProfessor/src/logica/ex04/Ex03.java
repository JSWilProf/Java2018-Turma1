package logica.ex04;

import br.senai.sp.info132.console.Programa;

public class Ex03 extends Programa {
	@Override
	public void inicio() {
		int a = leInteiro("Informe o 1º nº");
		int b = leInteiro("Informe o 2º nº");
		int c = leInteiro("Informe o 3º nº");

		double D = Math.pow(b, 2) - 4 * a * c;
		
		if(D >= 0) {
			double x = (-b + Math.sqrt(D)) / 2 * a;
			double x2 = (-b - Math.sqrt(D)) / 2 * a;
			escrevaL("x' : ", (int)x);
			escrevaL("x'': ", (int)x2);
		} else {
			escrevaL("A raiz é indeterminada");
		}
	}
}
