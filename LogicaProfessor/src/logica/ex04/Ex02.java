package logica.ex04;

import br.senai.sp.info132.console.Programa;

public class Ex02 extends Programa {
	@Override
	public void inicio() {
		while (true) {
			int a = leInteiro("Informe o tamanho da 1ª reta");
			int b = leInteiro("Informe o tamanho da 2ª reta");
			int c = leInteiro("Informe o tamanho da 3ª reta");

			if(Math.abs(b - c) < a  &&  b + c >= a &&
			   Math.abs(a - c) < b  &&  a + c >= b &&
			   Math.abs(a - b) < c  &&  a + b >= c ) {
			
				if (a == b && b == c) {
					escrevaL("Eqüilátero");
				} else if (a != b && a != c && b != c) {
					escrevaL("Escaleno");
				} else /* if (a == b || a == c || b == c) */ {
					escrevaL("Isósceles");
				}
			} else {
				escrevaL("Estas Retas não formam um Triângulo");
			}
		}
	}
}
