package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class Ex01 extends Programa {
	@Override
	public void inicio() {
		double larg = leReal("Informe a Largura");
		double comp = leReal("Informe o Comprimento");
		double prof = leReal("Informe a Profundidade");
		
		double preco = larg * comp * prof * 45;
		
		escrevaL("O Preco final é de R$", preco);
	}
}
