package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class Ex03a extends Programa {
	@Override
	public void inicio() {
		int[] nota = { 100, 50, 20, 10, 5, 2, 1 };
		
		int valor = leInteiro("Informe o Valor");

		for (int posicao = 0; posicao < nota.length; posicao++) {
			valor = listaNotas(valor, nota[posicao]);
		}
	}

	int listaNotas(int valor, int nota) {
		escrevaL("Notas de ", nota, ": ", valor / nota);
		return  valor % nota;
	}
}
