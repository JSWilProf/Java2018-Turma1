package logica.ex02;

import br.senai.sp.info132.console.Programa;

/*
 * Tabela de salarios
 * 
 * salários de   0 até  100 aumento de 10%
 * salários de 101 até  500 aumento de  7%
 * salários de 501 até 1000 aumento de  5%
 * salários acima   de 1000 aumento de  2%
 */

public class ExemploSe extends Programa {
	@Override
	public void inicio() {
		double salario = leReal("Informe o Salário");
		double novoSal;

		if (salario <= 100) {
			novoSal = salario * 1.1;
		} else if (salario <= 500) {
			novoSal = salario * 1.07;
		} else if (salario <= 1000) {
			novoSal = salario * 1.05;
		} else {
			novoSal = salario * 1.02;
		}

		escreva("Seu novo Salário é de R$ ", novoSal);
	}
}
