package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class Ex01a extends Programa {
	@Override
	public void inicio() {	
		escrevaL("O Preco final é de R$", 
				   leReal("Informe a Largura") *
				   leReal("Informe o Comprimento") *
				   leReal("Informe a Profundidade") *
			       45);
	}
}
