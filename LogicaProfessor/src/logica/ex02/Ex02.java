package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class Ex02 extends Programa {
	@Override
	public void inicio() {
		/*
		 
		    123  --> 321
		    12 3 --> 3
		    12.3 --> 3
		
		    12 --> 1.2 ==> 12 % 10 = 2
		    
		    123 --> 12.3 ==> int x = 123 / 10; ===> 12
		
		
		    C D U
		        3
		      3 0       3 ===> 30 === 3 * 10;
		      
		      3 0       30 =====> 32
		*/
		
		int num = leInteiro("Informe o Nº");
		int inv = 0;
		
		inv = inv * 10 + num % 10;
		num = num / 10;
		
		inv = inv * 10 + num % 10;
		num = num / 10;

		inv = inv * 10 + num % 10;
		num = num / 10;
		
		escrevaL("num: ", num ,"\ninv: ", inv);
	}
}
