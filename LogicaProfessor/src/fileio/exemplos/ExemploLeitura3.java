package fileio.exemplos;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExemploLeitura3 {
	public static void main(String[] args) {
		FileDialog tela = 
				new FileDialog((Frame)null, "Teste de Leitura", FileDialog.LOAD);
		tela.setVisible(true);
						
		try (FileInputStream arquivoCodificado = new FileInputStream(tela.getDirectory()+tela.getFile());	
			 InputStreamReader file =  new InputStreamReader(arquivoCodificado, "Cp1252");
			 BufferedReader arquivo = new BufferedReader(file) ) {
			
			String linha = arquivo.readLine();
			while(linha != null) {
				System.out.println(linha);
				
				linha = arquivo.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
