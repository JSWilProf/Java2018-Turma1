package fileio.exemplos;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ExemploLeitura2 {
	public static void main(String[] args) {
		FileDialog tela = new FileDialog((Frame) null, "Teste de Leitura", FileDialog.LOAD);
		tela.setVisible(true);

		try (FileReader file = new FileReader(tela.getDirectory() + tela.getFile()); 
			Scanner arquivo = new Scanner(file); ) {

			while (arquivo.hasNextLine()) {
				System.out.println(arquivo.nextLine());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
