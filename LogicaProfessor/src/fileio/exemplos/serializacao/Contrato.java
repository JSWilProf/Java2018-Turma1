package fileio.exemplos.serializacao;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Contrato implements Serializable {
	private static final long serialVersionUID = -2682010350866051915L;
	
	private int numero; 
	private String assunto = "";
	private Integer codigo;
	private boolean teste;

	public Contrato(int numero) {
		super();
		this.numero = numero;
	}

	public Contrato(int numero, String assunto) {
		this.numero = numero;
		this.assunto = assunto;
	}
	
	@Override
	public String toString() {
		return "numero: " + numero + "   assunto: " + assunto;
	}
}
