package fileio.exemplos.lombok;

import java.time.LocalDate;

import javax.swing.JOptionPane;

public class TesteLombok {
	public static void main(String[] args) {
		Album album = new Album();
		album.setId(1l);
		album.setNome("Bale Vista");
		album.setBanda("É Nóis");
		album.setDataDeLancamento(LocalDate.of(1994, 4, 20));
		
		JOptionPane.showMessageDialog(null, album);
	}
}
