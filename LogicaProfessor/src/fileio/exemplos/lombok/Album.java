package fileio.exemplos.lombok;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Album implements Comparable<Album>, Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nome;
	private String banda;
	private LocalDate dataDeLancamento;

	public Album(Long id) {
		this.id = id;
	}
	
	public Album(Long id, String nome, String banda) {
		super();
		this.id = id;
		this.nome = nome;
		this.banda = banda;
	}	

	@Override
	public int compareTo(Album outro) {
		return banda.toLowerCase().compareTo(outro.banda.toLowerCase());
	}

	@Override
	public String toString() {
		return String.format("Nome do Album: %s Banda: %s Lançamento: %3$Td/%3$Tm/%3$TY",
				nome, banda, dataDeLancamento); 
	}
}
