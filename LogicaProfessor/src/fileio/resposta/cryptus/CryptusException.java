package fileio.resposta.cryptus;


@SuppressWarnings("serial")
class CryptusException extends Exception {
	public CryptusException(String message) {
		super(message);
	}
}
