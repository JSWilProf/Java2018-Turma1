package relatorio.exemplos.albums.mdi.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import relatorio.exemplos.albums.mdi.model.Banda;

public class BandaDao {
	private Connection con;
	private PreparedStatement sqlInsert;
	private PreparedStatement sqlUpdate;
	private PreparedStatement sqlSelect;
	private PreparedStatement sqlDelete;
	private PreparedStatement sqlListar;
	private PreparedStatement sqlPesquisar;

 	public BandaDao() throws DaoException {
		try {
			// Registra o Driver JDBC
			Class.forName("com.mysql.jdbc.Driver");
			// Conecta ao Banco de Dados
			con = DriverManager
					.getConnection("jdbc:mysql://localhost:3307/projeto", "root", "root132");
			// Preparar as Declarações SQL
			prepararDeclaracoesSQL();
		} catch (ClassNotFoundException ex) {
			throw new DaoException("O Driver JDBC não foi encontrado");
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas ao conectar ao Banco de Dados");
		}
	}
	
	private void prepararDeclaracoesSQL() throws DaoException {
		try {
			// prepara a declaração para Inserir Album
			sqlInsert = con.prepareStatement("INSERT INTO Banda (nome) VALUES (?)");
			// prepara a declaração para Atualizar Album
			sqlUpdate = con.prepareStatement("UPDATE Banda SET nome=? WHERE idBanda=?");
			// prepara a declaração para Localizar Album
			sqlSelect = con.prepareStatement("SELECT * FROM Banda WHERE idBanda=?");
			// prepara a declaração para Localizar Album
			sqlDelete = con.prepareStatement("DELETE FROM Banda WHERE idBanda=?");
			// prepara a declaração para Listar IDS
			sqlListar = con.prepareStatement("SELECT idBanda FROM Banda ORDER BY nome");
			// prepara a declaração para Listar IDS
			sqlPesquisar = con.prepareStatement("SELECT idBanda FROM Banda WHERE nome like ? ORDER BY nome");
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na preparação das Declarações SQL");
		}
	}

	public void salvar(Banda obj) throws DaoException {
		try {
			if(obj.getId() == null) {
				sqlInsert.setString(1, obj.getNome());
				sqlInsert.execute();
			} else {
				sqlUpdate.setString(1, obj.getNome());
				sqlUpdate.setInt(2, obj.getId());
				sqlUpdate.execute();
			}
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas em Salvar/Atualizar um Banda");
		}
	}

	private Banda montaAlbum(ResultSet resultado) throws SQLException {
		Banda obj = new Banda();
		obj.setId(resultado.getInt("idBanda"));
		obj.setNome(resultado.getString("nome"));
		return obj;
	}
	
	public Banda localizar(int id) throws DaoException {
		try {
			sqlSelect.setInt(1, id);
			ResultSet resultSet = sqlSelect.executeQuery();
			if(resultSet.next())
				return montaAlbum(resultSet);
			else
				return null;
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na consulta de um Banda");
		}
	}
	
	public void remover(int id) throws DaoException {
		try {
			sqlDelete.setInt(1, id);
			sqlDelete.execute();
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas ao excluir um Banda");
		}
	}
	
	public List<Integer> listarIds() throws DaoException {
		try {
			List<Integer> lista = new ArrayList<>();
			ResultSet resultado = sqlListar.executeQuery();
			while(resultado.next()) {
				lista.add(resultado.getInt("idBanda"));
			}
			return lista;
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na consulta de um Banda");
		}
	}

	public List<Integer> pesquisarIds(String pesquisa) throws DaoException {
		try {
			List<Integer> lista = new ArrayList<>();
			if(!pesquisa.isEmpty()) {
				sqlPesquisar.setString(1, "%" + pesquisa + "%");
				ResultSet resultado = sqlPesquisar.executeQuery();
				while(resultado.next()) {
					lista.add(resultado.getInt("idBanda"));
				}
			}
			return lista;
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na consulta de um Banda");
		}
		
	}
}

