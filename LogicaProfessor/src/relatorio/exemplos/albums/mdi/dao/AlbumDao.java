package relatorio.exemplos.albums.mdi.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import relatorio.exemplos.albums.mdi.model.Album;
import relatorio.exemplos.albums.mdi.model.Banda;

public class AlbumDao {
	private Connection con;
	private PreparedStatement sqlInsert;
	private PreparedStatement sqlUpdate;
	private PreparedStatement sqlSelect;
	private PreparedStatement sqlDelete;
	private PreparedStatement sqlListar;
	private Banda banda;

 	public AlbumDao(Banda banda) throws DaoException {
 		this.banda = banda;
 		
		try {
			// Registra o Driver JDBC
			Class.forName("com.mysql.jdbc.Driver");
			// Conecta ao Banco de Dados
			con = DriverManager
					.getConnection("jdbc:mysql://localhost:3307/projeto", "root", "root132");
			// Preparar as Declarações SQL
			prepararDeclaracoesSQL();
		} catch (ClassNotFoundException ex) {
			throw new DaoException("O Driver JDBC não foi encontrado");
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas ao conectar ao Banco de Dados");
		}
	}
	
	private void prepararDeclaracoesSQL() throws DaoException {
		try {
			// prepara a declaração para Inserir Banda
			sqlInsert = con.prepareStatement("INSERT INTO Album (nome, idBanda, lancamento) VALUES (?, ?, ?)");
			// prepara a declaração para Atualizar Banda
			sqlUpdate = con.prepareStatement("UPDATE Album SET nome=?, lancamento=? WHERE idAlbum=?");
			// prepara a declaração para Localizar Banda
			sqlSelect = con.prepareStatement("SELECT * FROM Album WHERE idAlbum=?");
			// prepara a declaração para Localizar Banda
			sqlDelete = con.prepareStatement("DELETE FROM Album WHERE idAlbum=?");
			// prepara a declaração para Listar IDS
			sqlListar = con.prepareStatement("SELECT idAlbum FROM Album WHERE idBanda=? ORDER BY nome");
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na preparação das Declarações SQL");
		}
	}
	
	public void salvar(Album obj) throws DaoException {
		try {
			if(obj.getId() == null) {
				sqlInsert.setString(1, obj.getNome());
				sqlInsert.setInt(2, banda.getId());
				sqlInsert.setDate(3, new Date(obj.getLancamento().getTime()));
				sqlInsert.execute();
			} else {
				sqlUpdate.setString(1, obj.getNome());
				sqlUpdate.setDate(2, new Date(obj.getLancamento().getTime()));
				sqlUpdate.setInt(3, obj.getId());
				sqlUpdate.execute();
			}
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas em Salvar/Atualizar um Album");
		}
	}

	private Album montaAlbum(ResultSet resultado) throws SQLException {
		Album obj = new Album();
		obj.setId(resultado.getInt("idAlbum"));
		obj.setNome(resultado.getString("nome"));
		obj.setBanda(new Banda(resultado.getInt("idBanda")));
		obj.setLancamento(resultado.getDate("lancamento"));
		return obj;
	}
	
	public Album localizar(int id) throws DaoException {
		try {
			sqlSelect.setInt(1, id);
			ResultSet resultado = sqlSelect.executeQuery();
			if(resultado.next())
				return montaAlbum(resultado);
			else
				return null;
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na consulta de um Album");
		}
	}
	
	public void remover(int id) throws DaoException {
		try {
			sqlDelete.setInt(1, id);
			sqlDelete.execute();
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas ao excluir um Album");
		}
	}
	
	public List<Integer> listarIds() throws DaoException {
		try {
			List<Integer> lista = new ArrayList<>();
			sqlListar.setInt(1, banda.getId());
			ResultSet resultado = sqlListar.executeQuery();
			while(resultado.next()) {
				lista.add(resultado.getInt("idAlbum"));
			}
			return lista;
		} catch (SQLException ex) {
			ex.printStackTrace(System.err);
			throw new DaoException("Problemas na consulta de um Album");
		}
	}
}

