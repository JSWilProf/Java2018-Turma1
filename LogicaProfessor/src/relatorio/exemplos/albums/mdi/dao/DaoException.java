package relatorio.exemplos.albums.mdi.dao;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String message) {
		super(message);
	}
}
