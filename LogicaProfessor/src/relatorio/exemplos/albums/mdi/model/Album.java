package relatorio.exemplos.albums.mdi.model;

import java.util.Date;

import lombok.Data;

@Data
public class Album {
	private Integer id;
	private Banda banda;
	private String nome;
	private Date lancamento;
}
