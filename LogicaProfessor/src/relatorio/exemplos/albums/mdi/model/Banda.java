package relatorio.exemplos.albums.mdi.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Banda {
	private Integer id;
	private String nome;

	public Banda(Integer id) {
		super();
		this.id = id;
	}
}