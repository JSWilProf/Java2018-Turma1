package relatorio.exemplos.albums.mdi.tela.tablemodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import relatorio.exemplos.albums.mdi.dao.BandaDao;
import relatorio.exemplos.albums.mdi.dao.DaoException;
import relatorio.exemplos.albums.mdi.model.Banda;

@SuppressWarnings("serial")
public class BandaTableModel extends AbstractTableModel {
	private BandaDao dao;
	private String pesquisa;
	private Map<Integer, Integer> mapa;
	private String[] titulo = new String[] { "Nome" };

	public BandaTableModel(BandaDao dao) {
		this(dao, null);
	}
	
	public BandaTableModel(BandaDao dao, String pesquisa) {
		this.pesquisa = pesquisa;
		this.dao = dao;
		criarMapa();
	}

	public void pesquisar(String nome) {
		pesquisa = nome;
		fireTableDataChanged();
	}
	
	public int idDaBanda(int linha) {
		return mapa.get(linha);
	}
	
	@Override
	public void fireTableDataChanged() {
		criarMapa();
		super.fireTableDataChanged();
	}

	private void criarMapa() {
		mapa = new HashMap<>();
		try {
			int linha = 0;
			List<Integer> ids = pesquisa != null ? dao.pesquisarIds(pesquisa) : dao.listarIds();
				
			for (Integer id : ids) {
				mapa.put(linha++,  id);
			}
		} catch(DaoException ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public int getRowCount() {
		return mapa.size();
	}

	@Override
	public int getColumnCount() {
		return titulo.length;
	}
	
	@Override
	public String getColumnName(int col) {
		return titulo[col];
	}

	@Override
	public Object getValueAt(int linha, int col) {
		Object value = null;
		try {
			Banda banda = dao.localizar(mapa.get(linha));
			switch (col) {
			case 0:
				value = banda.getNome();
				break;
			}
		} catch (DaoException ex) {
			ex.printStackTrace();
		}
		return value;
	}

}
