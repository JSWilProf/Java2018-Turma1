package relatorio.exemplos.albums.mdi.tela.tablemodel;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import relatorio.exemplos.albums.mdi.dao.AlbumDao;
import relatorio.exemplos.albums.mdi.dao.DaoException;
import relatorio.exemplos.albums.mdi.model.Album;

@SuppressWarnings("serial")
public class AlbumTableModel extends AbstractTableModel {
	private AlbumDao dao;
	private Map<Integer, Integer> mapa;
	private String[] titulo = new String[] { "Nome", "Data de Lançamento" };
	private static DateFormat fmt = DateFormat.getDateInstance(DateFormat.LONG);

	public AlbumTableModel(AlbumDao dao) {
		this.dao = dao;
		criarMapa();
	}

	@Override
	public void fireTableDataChanged() {
		criarMapa();
		super.fireTableDataChanged();
	}

	private void criarMapa() {
		mapa = new HashMap<>();
		try {
			int linha = 0;
			for (Integer id : dao.listarIds()) {
				mapa.put(linha++,  id);
			}
		} catch(DaoException ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public int getRowCount() {
		return mapa.size();
	}

	@Override
	public int getColumnCount() {
		return titulo.length;
	}
	
	@Override
	public String getColumnName(int col) {
		return titulo[col];
	}

	@Override
	public Object getValueAt(int linha, int col) {
		Object value = null;
		try {
			Album banda = dao.localizar(mapa.get(linha));
			switch (col) {
			case 0:
				value = banda.getNome();
				break;
			case 1:
				value = fmt.format(banda.getLancamento());
			}
		} catch (DaoException ex) {
			ex.printStackTrace();
		}
		return value;
	}

}
