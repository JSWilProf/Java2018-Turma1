package relatorio.exemplos.albums.mdi.tela;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import relatorio.exemplos.albums.mdi.dao.AlbumDao;
import relatorio.exemplos.albums.mdi.dao.DaoException;
import relatorio.exemplos.albums.mdi.model.Album;
import relatorio.exemplos.albums.mdi.model.Banda;
import relatorio.exemplos.albums.mdi.tela.tablemodel.AlbumTableModel;

import javax.swing.JScrollPane;
import javax.swing.JTable;


@SuppressWarnings("serial")
public class CadAlbuns extends JInternalFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblLancamento;
	private JTextField tfLancamento;
	private JButton btnGravar;
	private JButton btnSair;
	private AlbumDao dao;
	private static DateFormat fmt = DateFormat.getDateInstance(DateFormat.DEFAULT, new Locale("pt", "BR"));
	private JScrollPane scrollPane;
	private JTable table;
	private AlbumTableModel model; 

	public CadAlbuns(Banda banda) throws DaoException {
		dao = new AlbumDao(banda);
		model = new AlbumTableModel(dao);
		
		setTitle("Cadastro de Album");
		setBounds(100, 100, 467, 362);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblNome = new JLabel("Nome do Album");
		lblNome.setBounds(16, 25, 114, 16);
		contentPane.add(lblNome);

		tfNome = new JTextField();
		tfNome.setBounds(142, 19, 285, 28);
		contentPane.add(tfNome);
		tfNome.setColumns(10);

		lblLancamento = new JLabel("Data de Lançamento");
		lblLancamento.setBounds(16, 64, 141, 16);
		contentPane.add(lblLancamento);

		tfLancamento = new JTextField();
		tfLancamento.setBounds(169, 58, 154, 28);
		contentPane.add(tfLancamento);
		tfLancamento.setColumns(10);

		btnGravar = new JButton("Gravar");
		btnGravar.addActionListener(this);
		btnGravar.setBounds(16, 103, 117, 29);
		contentPane.add(btnGravar);

		btnSair = new JButton("Fechar");
		btnSair.addActionListener(this);
		btnSair.setBounds(310, 103, 117, 29);
		contentPane.add(btnSair);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(16, 144, 406, 152);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(model);
		scrollPane.setViewportView(table);
	}

	public void actionPerformed(ActionEvent ev) {
		String cmd = ev.getActionCommand();

		try {
			if (cmd.equals("Gravar")) {
				Album obj  = new Album();
				obj.setNome(tfNome.getText());
				obj.setLancamento(fmt.parse(tfLancamento.getText()));
				
				dao.salvar(obj);
				model.fireTableDataChanged();
				
				tfNome.setText("");
				tfLancamento.setText("");
				tfNome.requestFocus();
			} else {
				setVisible(false);
			}
		} catch (ParseException ex) {
			JOptionPane.showInternalMessageDialog(this, "Data Inválida");
		} catch (DaoException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage());
		}
	}
}
