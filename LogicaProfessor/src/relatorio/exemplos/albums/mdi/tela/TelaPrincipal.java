package relatorio.exemplos.albums.mdi.tela;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;

import relatorio.exemplos.albums.mdi.relatorio.CriaRelatorio;
import relatorio.pedidos.TipoDeImpressao;

@SuppressWarnings("serial")
public class TelaPrincipal extends JFrame implements ActionListener {
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnCadastro;
	private JMenuItem mntmBandas;
	private JMenuItem mntmImprimir;
	private JSeparator separator;
	private JMenuItem mntmSair;
	private JDesktopPane desktopPane;
	private JMenuItem mntmAlbuns;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaPrincipal() {
		setTitle("Cadastro de Albuns de Bandas Musicais");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 554, 473);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnCadastro = new JMenu("Cadastro");
		menuBar.add(mnCadastro);
		
		mntmBandas = new JMenuItem("Bandas");
		mntmBandas.addActionListener(this);
		mnCadastro.add(mntmBandas);
		
		mntmImprimir = new JMenuItem("Impress\u00E3o");
		mntmImprimir.addActionListener(this);
		
		mntmAlbuns = new JMenuItem("Albuns");
		mntmAlbuns.addActionListener(this);
		mnCadastro.add(mntmAlbuns);
		mnCadastro.add(mntmImprimir);
		
		separator = new JSeparator();
		mnCadastro.add(separator);
		
		mntmSair = new JMenuItem("Sair");
		mntmSair.addActionListener(this);
		mnCadastro.add(mntmSair);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.WHITE);
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}

	public void actionPerformed(ActionEvent ev) {
		Object cmd = ev.getSource();
	
		try {
			if(cmd.equals(mntmBandas)) {
				CadBandas tela = new CadBandas();
				desktopPane.add(tela);
				tela.setVisible(true);
			} else if(cmd.equals(mntmAlbuns)) {
				SelecionaBanda tela = new SelecionaBanda();
				desktopPane.add(tela);
				tela.setVisible(true);
			} else if(cmd.equals(mntmImprimir)) {
				TipoDeImpressao tipo = (TipoDeImpressao)JOptionPane.
						showInputDialog(null, "Selecione o Tipo de Relatório", "Imprimir",
						JOptionPane.QUESTION_MESSAGE, null, TipoDeImpressao.values(), TipoDeImpressao.pdf);
				InputStream report = CriaRelatorio.class.getResourceAsStream("relalbuns.rptdesign");
				File relatorio = File.createTempFile("Album", ".pdf");
				CriaRelatorio.executeReport(report, relatorio.getAbsolutePath(), tipo);
			    if(Desktop.isDesktopSupported()) {
			    		Desktop.getDesktop().open(relatorio);
			    } else {
			    		JOptionPane.showMessageDialog(this, "Desktop não é suportado");
			    }
			} else {
				System.exit(0);
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage());
		}
	}
}
