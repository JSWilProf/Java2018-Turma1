package relatorio.exemplos.albums.mdi.tela;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import relatorio.exemplos.albums.mdi.dao.BandaDao;
import relatorio.exemplos.albums.mdi.dao.DaoException;
import relatorio.exemplos.albums.mdi.model.Banda;
import relatorio.exemplos.albums.mdi.tela.tablemodel.BandaTableModel;

@SuppressWarnings("serial")
public class SelecionaBanda extends JInternalFrame implements ActionListener {
	private JLabel lblPesquisa;
	private JTextField tfNome;
	private JScrollPane scrollPane;
	private JButton btnPesquisar;
	private JTable table;
	private BandaDao dao;
	private BandaTableModel model;
	private JButton btnSelecionar;
	private JButton btnFechar;

	public SelecionaBanda() throws DaoException {
		dao = new BandaDao();
		model = new BandaTableModel(dao, "");
		
		setTitle("Pesquisar Bandas Musicais");
		setBounds(100, 100, 450, 329);
		
		lblPesquisa = new JLabel("Nome da Banda");
		lblPesquisa.setBounds(6, 11, 97, 16);
		
		tfNome = new JTextField();
		tfNome.setBounds(115, 6, 186, 26);
		tfNome.setColumns(10);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 41, 404, 196);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(this);
		btnPesquisar.setBounds(307, 6, 104, 29);
		getContentPane().setLayout(null);
		
		table = new JTable();
		table.setModel(model);
		
		scrollPane.setViewportView(table);
		getContentPane().add(scrollPane);
		getContentPane().add(lblPesquisa);
		getContentPane().add(tfNome);
		getContentPane().add(btnPesquisar);
		
		btnSelecionar = new JButton("Selecionar");
		btnSelecionar.addActionListener(this);
		btnSelecionar.setBounds(6, 248, 117, 29);
		getContentPane().add(btnSelecionar);
		
		btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(this);
		btnFechar.setBounds(293, 248, 117, 29);
		getContentPane().add(btnFechar);
	}
	
	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		try {
			if(botao.equals(btnPesquisar)) {
				String nome = tfNome.getText();
				
				if(!nome.isEmpty()) {
					model.pesquisar(nome);
				}
			} else if(botao.equals(btnSelecionar)) {
				int linha = table.getSelectedRow();
				if(linha >= 0) { // uma linha no JTable foi selecionada
					if(table.getAutoCreateRowSorter()) { // Tem odenador de linhas
						// então converta a linha da View para o Model
						linha = table.getRowSorter().convertRowIndexToModel(linha);
					}
					Banda banda = dao.localizar(model.idDaBanda(linha));
					CadAlbuns dialog = new CadAlbuns(banda);
					getDesktopPane().add(dialog);
					dialog.setVisible(true);
				}
			} else {
				setVisible(false);
			}
		} catch (DaoException ex) {
			JOptionPane.showInternalMessageDialog(this, ex.getMessage());
		}
	}
}
