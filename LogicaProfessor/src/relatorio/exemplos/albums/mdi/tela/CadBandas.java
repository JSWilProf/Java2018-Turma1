package relatorio.exemplos.albums.mdi.tela;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import relatorio.exemplos.albums.mdi.dao.BandaDao;
import relatorio.exemplos.albums.mdi.dao.DaoException;
import relatorio.exemplos.albums.mdi.model.Banda;
import relatorio.exemplos.albums.mdi.tela.tablemodel.BandaTableModel;

import javax.swing.JScrollPane;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class CadBandas extends JInternalFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JButton btnGravar;
	private JButton btnSair;
	private JScrollPane scrollPane;
	private JTable table;
	private BandaDao dao;
	private BandaTableModel model;

	public CadBandas() throws DaoException {
		dao = new BandaDao();
		model = new BandaTableModel(dao);
		
		setTitle("Cadastro da Banda");
		setBounds(100, 100, 382, 304);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblNome = new JLabel("Nome da Banda");
		lblNome.setBounds(16, 25, 98, 16);
		contentPane.add(lblNome);

		tfNome = new JTextField();
		tfNome.setBounds(126, 19, 214, 28);
		contentPane.add(tfNome);
		tfNome.setColumns(10);

		btnGravar = new JButton("Gravar");
		btnGravar.addActionListener(this);
		btnGravar.setBounds(16, 66, 117, 29);
		contentPane.add(btnGravar);

		btnSair = new JButton("Fechar");
		btnSair.addActionListener(this);
		btnSair.setBounds(223, 66, 117, 29);
		contentPane.add(btnSair);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(16, 107, 321, 133);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(model);
		scrollPane.setViewportView(table);
	}

	public void actionPerformed(ActionEvent ev) {
		String cmd = ev.getActionCommand();

		try {
			if (cmd.equals("Gravar")) {
				Banda obj  = new Banda();
				obj.setNome(tfNome.getText());
				
				dao.salvar(obj);
				model.fireTableDataChanged();
				
				tfNome.setText("");
				tfNome.requestFocus();
			} else {
				setVisible(false);
			}
		} catch (DaoException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage());
			ex.printStackTrace();
		}
	}
}
