package relatorio.pedidos;

public enum TipoDeImpressao {
	pdf, html, docx, pptx, xlsx;
}
