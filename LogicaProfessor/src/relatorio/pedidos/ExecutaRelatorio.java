package relatorio.pedidos;

import java.awt.Desktop;
import java.io.File;
import java.io.InputStream;

import javax.swing.JOptionPane;

public class ExecutaRelatorio {
	public static void main(String[] args) throws Exception {
		// Solicita que seja escolhido o Tipo do formato do Relatório
		TipoDeImpressao tipo = (TipoDeImpressao)JOptionPane.
				showInputDialog(null, "Selecione o Tipo de Relatório", "Imprimir",
				JOptionPane.QUESTION_MESSAGE, null, TipoDeImpressao.values(), TipoDeImpressao.pdf);
		
		// Localiza e abre o arquivo do modelo do relatório
		InputStream report = CriaRelatorio.class.getResourceAsStream("pedido.rptdesign");
		
		// Cria um arquivo temporário para armazenar o relatório  
		File saida = File.createTempFile("pedidos", "."+tipo);
		
		// Invoca a engine do BIRT para processar o modelo do relatório
		// e produzir o mesmo no formato solicitado
		CriaRelatorio.executeReport(report, saida.getAbsolutePath(), tipo);
		
		// Solicita ao sistema operacional que o arquivo temporário
		// contendo o relatório seja aberto pelo programa correspondente
		// ao seu formato (Ex. Acrobat Reader -> PDF; Word -> DOC, etc)
	    if(Desktop.isDesktopSupported()) {
	    		Desktop.getDesktop().open(saida);
	    } else {
	    		JOptionPane.showMessageDialog(null, "Desktop não é suportado");
	    }

	}
}
