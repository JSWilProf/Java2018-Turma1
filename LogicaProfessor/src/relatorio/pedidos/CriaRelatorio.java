package relatorio.pedidos;

import java.io.InputStream;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.DocxRenderOption;
import org.eclipse.birt.report.engine.api.EXCELRenderOption;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IPDFRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;

public class CriaRelatorio {
	private static final String LIB_DIR = "../../relatorio_lib";
	
	public static void executeReport(InputStream report, String saida, TipoDeImpressao tipo) throws Exception {
		try {
			EngineConfig config = new EngineConfig();
			config.setBIRTHome(LIB_DIR );
			config.setLogConfig(LIB_DIR + "/Logs", java.util.logging.Level.OFF);

			Platform.startup(config);
			IReportEngineFactory factory = 
				(IReportEngineFactory) Platform.
					createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
			IReportEngine engine = factory.createReportEngine(config);

			IReportRunnable design = engine.openReportDesign(report);
			IRunAndRenderTask task = engine.createRunAndRenderTask(design);

			RenderOption options;
			
			if(tipo.equals(TipoDeImpressao.pdf)) {
				PDFRenderOption op = new PDFRenderOption();
				op.setOption(IPDFRenderOption.PAGE_OVERFLOW, IPDFRenderOption.FIT_TO_PAGE_SIZE);
				op.setOutputFileName(saida);
				op.setOutputFormat("pdf");
				options = op;
			} else if(tipo.equals(TipoDeImpressao.html)) {
				HTMLRenderOption op = new HTMLRenderOption();
				op.setOutputFileName(saida);
				op.setOutputFormat("html");
				op.setHtmlRtLFlag(false);
				op.setEmbeddable(false);
				options = op;
			} else if(tipo.equals(TipoDeImpressao.pptx)) {
				HTMLRenderOption op = new HTMLRenderOption();
				op.setOutputFileName(saida);
				op.setOutputFormat("pptx");
				options = op;
			} else if(tipo.equals(TipoDeImpressao.xlsx)) {
				EXCELRenderOption op = new EXCELRenderOption(); 
				op.setOutputFileName(saida);
				op.setOutputFormat("xlsx");
				options = op;
			} else {
				DocxRenderOption op = new DocxRenderOption();
				op.setOutputFileName(saida);
				op.setOutputFormat("docx");
				options = op;
			}
			
			task.setRenderOption(options);
			task.run();
			task.close();
			engine.destroy();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception("Problemas com a geração do relatório");
		} finally {
			Platform.shutdown();
		}
	}
}
