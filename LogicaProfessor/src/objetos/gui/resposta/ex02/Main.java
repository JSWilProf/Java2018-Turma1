package objetos.gui.resposta.ex02;

import java.awt.EventQueue;

import objetos.gui.resposta.ex02.view.CadCliente;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadCliente frame = new CadCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
