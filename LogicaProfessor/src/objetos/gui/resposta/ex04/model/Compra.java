package objetos.gui.resposta.ex04.model;

import java.util.Date;
import java.util.List;

public class Compra {
	private Date data;
	private List<ItemDeCompra> itens;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<ItemDeCompra> getItens() {
		return itens;
	}

	public void setItens(List<ItemDeCompra> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		String lista = "";
		for (ItemDeCompra item : itens)
			lista += item + "\n";
		
		return "Data: " + data + "\n" + lista;
	}

}
