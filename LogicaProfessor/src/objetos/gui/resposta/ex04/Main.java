package objetos.gui.resposta.ex04;

import java.awt.EventQueue;

import objetos.gui.resposta.ex04.view.TelaCompra;

public class Main {	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCompra frame = new TelaCompra();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
