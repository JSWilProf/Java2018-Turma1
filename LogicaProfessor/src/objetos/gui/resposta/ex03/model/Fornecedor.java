package objetos.gui.resposta.ex03.model;

import java.util.ArrayList;
import java.util.List;

public class Fornecedor {
	private Integer id;
	private String nome;
	private Endereco endereco;
	private List<Produto> produtos = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		String prods = "";
		for (Produto produto : produtos)
			prods += produto + "\n";
		
		return "Nome: " + nome + "\n" + endereco + "\n" + prods;
	}

}
