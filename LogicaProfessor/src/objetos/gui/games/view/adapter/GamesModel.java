package objetos.gui.games.view.adapter;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import objetos.gui.games.model.Game;

@SuppressWarnings("serial")
public class GamesModel extends AbstractTableModel {
	private String[] nomesColunas = new String[] { "Nome", "Plataforma", "Ano" };
	private List<Game> lista;

	public  GamesModel(List<Game> lista) {
		this.lista = lista;
	}

	public void adicionar(Game jogo) {
		lista.add(jogo);
		fireTableDataChanged(); // atualizar a tela do JTable
	}
	
	public void remover(int linha) {
		lista.remove(linha);
		fireTableDataChanged(); // atualizar a tela do JTable
	}

	@Override
	public int getRowCount() {
		return lista.size();
	}

	@Override
	public int getColumnCount() {
		return nomesColunas.length;
	}

	@Override
	public String getColumnName(int col) {
		return nomesColunas[col];
	}

	@Override
	public Object getValueAt(int linha, int col) {
		String valor = null;
		Game game = lista.get(linha); 
		
		switch (col) {
		case 0:
			valor = game.getNome();
			break;
		case 1:
			valor = game.getPlataforma();
			break;
		case 2:
			valor = String.valueOf(game.getAno());
			break;
		}
		
		return valor;
	}



}
