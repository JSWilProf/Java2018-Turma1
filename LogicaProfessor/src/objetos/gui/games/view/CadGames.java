package objetos.gui.games.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import objetos.gui.games.model.Game;

@SuppressWarnings("serial")
public class CadGames extends JDialog implements ActionListener {
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblPlataforma;
	private JTextField tfPlataforma;
	private JLabel lblAnoDeLanamento;
	private JTextField tfAno;
	private JButton btnSalvar;
	private JButton btnFechar;
	
	private Game jogo;
	
	public CadGames(Game jogo) {
		setResizable(false);
		setModal(true);
		setTitle("Cadastro de Games");
		setBounds(100, 100, 439, 220);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		lblPlataforma = new JLabel("Plataforma");
		
		tfPlataforma = new JTextField();
		tfPlataforma.setColumns(10);
		
		lblAnoDeLanamento = new JLabel("Ano de Lançamento");
		
		tfAno = new JTextField();
		tfAno.setColumns(10);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnFechar = new JButton("Cancelar");
		btnFechar.addActionListener(this);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblAnoDeLanamento)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(tfAno, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblPlataforma)
							.addGap(18)
							.addComponent(tfPlataforma, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
								.addComponent(btnSalvar)
								.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnFechar))
							.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
								.addComponent(lblNome)
								.addGap(18)
								.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(16)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPlataforma)
						.addComponent(tfPlataforma, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAnoDeLanamento)
						.addComponent(tfAno, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 107, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnFechar))
					.addGap(18))
		);
		getContentPane().setLayout(groupLayout);

		atualizaTela(jogo);
	}

	public void atualizaTela(Game jogo) {
		this.jogo = jogo;
		tfNome.setText(jogo.getNome());
		tfPlataforma.setText(jogo.getPlataforma());
		tfAno.setText(String.valueOf(jogo.getAno()));
	}
	
	public Game getJogo() {
		return jogo;
	}
 	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		
		if(botao.equals(btnSalvar)) {
			jogo.setNome(tfNome.getText());
			jogo.setPlataforma(tfPlataforma.getText());
			jogo.setAno(Integer.parseInt(tfAno.getText()));
		} else {
			jogo = null;
		}
		
		setVisible(false);
	}
}
