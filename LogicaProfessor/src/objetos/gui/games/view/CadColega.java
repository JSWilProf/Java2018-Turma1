package objetos.gui.games.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import objetos.gui.games.model.Colega;
import objetos.gui.games.model.Game;
import objetos.gui.games.view.adapter.GamesModel;

@SuppressWarnings("serial")
public class CadColega extends JFrame implements ActionListener {
	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblEmail;
	private JTextField tfEmail;
	private JButton btnSalvar;
	private JButton btnSair;
	private JButton btnListar;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btAdicionar;
	private JButton btRemover;

	// Cadastro de Colegas em Memória
	private List<Colega> cadastro = new ArrayList<>();

	// Lista dos Games de UM (1) Colega
	private List<Game> jogos;
	
	// Provê os dados de games para o JTable
	private GamesModel model;

	public CadColega() {
		setTitle("Cadastro de Colegas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 426, 349);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		lblNome = new JLabel("Nome");

		tfNome = new JTextField();
		tfNome.setColumns(10);

		lblEmail = new JLabel("E-Mail");

		tfEmail = new JTextField();
		tfEmail.setColumns(10);

		btnSalvar = new JButton("Salvar");
		btnSalvar.setMnemonic('S');
		btnSalvar.addActionListener(this);

		btnSair = new JButton("Sair");
		btnSair.setMnemonic('r');
		btnSair.addActionListener(this);

		btnListar = new JButton("Listar");
		btnListar.setMnemonic('l');
		btnListar.addActionListener(this);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Games", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		scrollPane = new JScrollPane();
		
		btAdicionar = new JButton("+");
		btAdicionar.addActionListener(this);
		
		btRemover = new JButton("-");
		btRemover.addActionListener(this);
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(lblNome)
								.addGap(18)
								.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 347, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(lblEmail)
								.addGap(18)
								.addComponent(tfEmail)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnSalvar)
							.addPreferredGap(ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
							.addComponent(btnListar)
							.addGap(85)
							.addComponent(btnSair)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEmail)
						.addComponent(tfEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnSair)
						.addComponent(btnListar))
					.addContainerGap())
		);
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btAdicionar, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btAdicionar)
						.addComponent(btRemover))
					.addContainerGap())
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(table);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);

		configuraTabela();
		
		// Define o botão OK como o botão padrão do formulario (Que aceita o ENTER)
		getRootPane().setDefaultButton(btnSalvar);
		// Centraliza a Janela no Centro da tela do computador
		setLocationRelativeTo(null);
	}

	private void configuraTabela() {
		jogos = new ArrayList<>();
		model = new GamesModel(jogos);
		table.setModel(model);
	}
	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();

		if (botao.equals(btnSalvar)) { // Ok
			String nome = tfNome.getText();
			String email = tfEmail.getText();

			// Criar o "registro" Objeto do Colega
			Colega obj = new Colega();
			obj.setNome(nome);
			obj.setEmail(email);
			obj.setJogos(jogos);

			// Salva os dados do Colega
			cadastro.add(obj);
			
			configuraTabela();
			
			tfNome.setText("");
			tfEmail.setText("");
			tfNome.requestFocus();
		} else if(botao.equals(btnListar)) {
			String msg = "Cadastro de Colegas\n\n";
			for (Colega colega : cadastro) {
				msg += colega + "\n";
			}
			JOptionPane.showMessageDialog(this, msg);
		} else if(botao.equals(btAdicionar)) {
			Game jogo = new Game();
			
			CadGames tela = new CadGames(jogo);
			tela.setLocationRelativeTo(this);
			tela.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			tela.setVisible(true);
			
			if(tela.getJogo() != null) {
				model.adicionar(jogo);
			}
		} else if(botao.equals(btRemover)) {
			int linha = table.getSelectedRow();
			if(linha != -1) {
				if(table.getRowSorter() != null) {
					linha = table.getRowSorter().convertRowIndexToModel(linha);
				}
				
				int op = JOptionPane.showConfirmDialog(this, 
						"Confirma excluir o Game", "Excluir Game",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(op == 0)
					model.remover(linha);
			} else {
				JOptionPane.showMessageDialog(this, "Selecione um Game");
			}
		} else { // Sair
			System.exit(0);
		}
	}
}
