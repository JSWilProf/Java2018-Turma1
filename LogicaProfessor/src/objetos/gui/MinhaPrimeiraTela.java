package objetos.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MinhaPrimeiraTela {
	public static void main(String[] args) {
		JFrame tela = new JFrame("Minha 1ª Tela");

		JPanel painel1 = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		painel1.add(new JLabel("Nome"));
		JTextField tfNome = new JTextField(10);
		painel1.add(tfNome);
		tela.add(painel1, BorderLayout.CENTER);

		JPanel painel2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		JButton btOk = new JButton("OK");
		tela.getRootPane().setDefaultButton(btOk);
		btOk.addActionListener((e) -> {
			String nome = tfNome.getText();
			JOptionPane.showMessageDialog(null, "Bem Vindo " + nome);
			tfNome.setText("");
			tfNome.requestFocus();
		});
		painel2.add(btOk);
		JButton btSair = new JButton("Sair");
		btSair.addActionListener((e) -> System.exit(0));
		painel2.add(btSair);
		tela.add(painel2, BorderLayout.SOUTH);

		tela.pack();
		tela.setResizable(false);
		tela.setLocationRelativeTo(null);
		tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tela.setVisible(true);
	}
}
