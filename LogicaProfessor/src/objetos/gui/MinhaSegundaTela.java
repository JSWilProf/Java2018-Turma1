package objetos.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class MinhaSegundaTela extends JFrame implements ActionListener {
	private JTextField tfNome = new JTextField(10);
	private JButton btOk = new JButton("OK");
	private JButton btSair = new JButton("Sair");

	public MinhaSegundaTela() {
		setTitle("Minha 1ª Tela");
		JPanel painel1 = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		painel1.add(new JLabel("Nome"));
		
		painel1.add(tfNome);
		add(painel1, BorderLayout.CENTER);

		JPanel painel2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		getRootPane().setDefaultButton(btOk);
		btOk.addActionListener(this);
		painel2.add(btOk);
		btSair.addActionListener(this);
		painel2.add(btSair);
		add(painel2, BorderLayout.SOUTH);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		
		if(botao.equals(btOk)) { // Ok
			String nome = tfNome.getText();
			JOptionPane.showMessageDialog(null, "Bem Vindo " + nome);
			tfNome.setText("");
			tfNome.requestFocus();
		} else { // Sair
			System.exit(0);
		}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new MinhaSegundaTela());
	}

}
