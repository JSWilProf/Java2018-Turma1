package objetos.gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

public class ExemploProgFuncional {
	public static void main(String[] args) {
		List<Compra> compras = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			int dia = (int)Math.ceil(Math.random() * 30);
			compras.add(new Compra(LocalDate.of(2018, 04, dia), "Prod "+ dia, dia, dia));
		}
		
		String msg = "Cadastro de Fornecedores\n\n";
	    for(Compra compra : compras) {
	    	String txt = compra.toString();
			msg += txt + "\n";
	    }
		JOptionPane.showMessageDialog(null, msg);
		
		JOptionPane.showMessageDialog(null, 
				"Cadastro de Fornecedores\n\n" +      // <=== msg
				compras.stream() 				      // <=== for(Compra compra : compras)
				.map((compra) -> compra.toString())   // <=== String txt = compra.toString();
				.collect(Collectors.joining("\n")));  // <=== msg += txt + "\n";
	}
}

class Compra {
	private LocalDate data;
	private String produto;
	private double preco;
	private int quantidade;

	public Compra() {
		super();
	}

	public Compra(LocalDate data, String produto, double preco, int quantidade) {
		super();
		this.data = data;
		this.produto = produto;
		this.preco = preco;
		this.quantidade = quantidade;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

}