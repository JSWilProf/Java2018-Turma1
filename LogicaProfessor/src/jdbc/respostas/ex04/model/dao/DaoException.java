package jdbc.respostas.ex04.model.dao;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String msg) {
		super(msg);
	}
}
