-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema prjgn1801
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema prjgn1801
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `prjgn1801` DEFAULT CHARACTER SET latin1 ;
USE `prjgn1801` ;

-- -----------------------------------------------------
-- Table `prjgn1801`.`endereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prjgn1801`.`endereco` (
  `idendereco` INT(11) NOT NULL AUTO_INCREMENT,
  `logradouro` VARCHAR(180) NOT NULL,
  `numero` VARCHAR(10) NOT NULL,
  `bairro` VARCHAR(100) NOT NULL,
  `cep` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`idendereco`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `prjgn1801`.`fornecedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prjgn1801`.`fornecedor` (
  `idfornecedor` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `idendereco` INT(11) NOT NULL,
  PRIMARY KEY (`idfornecedor`),
  INDEX `fk_fornecedor_endereco_idx` (`idendereco` ASC),
  CONSTRAINT `fk_fornecedor_endereco`
    FOREIGN KEY (`idendereco`)
    REFERENCES `prjgn1801`.`endereco` (`idendereco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `prjgn1801`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prjgn1801`.`produto` (
  `idproduto` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `preco` DOUBLE NOT NULL,
  `idfornecedor` INT(11) NOT NULL,
  PRIMARY KEY (`idproduto`),
  INDEX `fk_produto_fornecedor1_idx` (`idfornecedor` ASC),
  CONSTRAINT `fk_produto_fornecedor`
    FOREIGN KEY (`idfornecedor`)
    REFERENCES `prjgn1801`.`fornecedor` (`idfornecedor`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prjgn1801`.`compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prjgn1801`.`compra` (
  `idcompra` INT NOT NULL AUTO_INCREMENT,
  `data` DATE NOT NULL,
  PRIMARY KEY (`idcompra`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prjgn1801`.`itemdecompra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prjgn1801`.`itemdecompra` (
  `iditemdecompra` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `quantidade` INT NOT NULL,
  `valor` DOUBLE NOT NULL,
  `idcompra` INT NOT NULL,
  PRIMARY KEY (`iditemdecompra`),
  INDEX `fk_itemdecompra_compra1_idx` (`idcompra` ASC),
  CONSTRAINT `fk_itemdecompra_compra1`
    FOREIGN KEY (`idcompra`)
    REFERENCES `prjgn1801`.`compra` (`idcompra`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
