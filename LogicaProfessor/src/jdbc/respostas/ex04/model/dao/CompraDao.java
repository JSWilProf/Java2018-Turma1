package jdbc.respostas.ex04.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.respostas.ex04.model.Compra;
import jdbc.respostas.ex04.model.ItemDeCompra;

public class CompraDao {
	private static CompraDao instance;
	private Connection con;
	private PreparedStatement insere;
	private PreparedStatement altera;
	private PreparedStatement remove;
	private PreparedStatement consulta;
	private PreparedStatement queryId;	

	private CompraDao() throws DaoException {
		try {
			con = DatabaseManager.getConnection();
	
			consulta = con.prepareStatement("SELECT * FROM compra");
			insere = con.prepareStatement("INSERT INTO compra (data) VALUES (?)");
			altera = con.prepareStatement("UPDATE compra set data=? WHERE idcompra=?");
			remove = con.prepareStatement("DELETE compra WHERE idcompra=?");
			queryId = con.prepareStatement("select last_insert_id() as id"); 
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("A conexão com o Banco de Dados foi estabelecida");
		}
	}
	
	public static CompraDao getInstance() throws DaoException {
		if(instance == null) {
			instance = new CompraDao();
		}
		
		return instance;
	}
	
	private void inserir(Compra compra) throws SQLException, DaoException {
		// Salva a Compra
		insere.setDate(1, new java.sql.Date(compra.getData().getTime()));
		insere.execute();

		ResultSet result = queryId.executeQuery();
		if(result.next()) {
			compra.setId(result.getInt("id")); 
		} else {
			con.rollback();
			con.setAutoCommit(true);
			throw new DaoException("Falha ao salver o Compra");
		}
		
		// Salva os Produtos
		ItemDeCompraDao dao = ItemDeCompraDao.getInstance();
		for(ItemDeCompra item : compra.getItens()) {
			dao.salvar(item, compra.getId());
		}	
	}
	
	private void atualizar(Compra compra) throws SQLException, DaoException {
		// Atualiza o Fornecedor
		altera.setDate(1, new java.sql.Date(compra.getData().getTime()));
		altera.setInt(2, compra.getId());
		altera.execute();
		
		// Atualiza os Produtos
		ItemDeCompraDao dao = ItemDeCompraDao.getInstance();
		for(ItemDeCompra item : compra.getItens()) {
			dao.salvar(item, compra.getId());
		}	
	}
	
	public Compra salvar(Compra compra) throws DaoException {
		String acao = "incluir";
		try {
			con.setAutoCommit(false);

			if(compra.getId() == null) { // Incluir
				inserir(compra);	
				con.commit();
			} else {                   // Atualizar
				acao = "atualizar";
				atualizar(compra);
				con.commit();
			}
			return compra;
		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				con.rollback();
				con.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new DaoException("Falha ao " + acao + " o Compra");					
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void remover(int id) throws DaoException {
		try {
			// Foi configurado cascade on delete no Banco de Dados
			// assim ao excluir um Fornecedor todos os Produtos 
			// serão excluidos também
			remove.setInt(1, id);
			remove.execute();			
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao excluir o Compra");
		}		
	}

	public List<Compra> listar() throws DaoException {
		try {
			List<Compra> compras = new ArrayList<>();
			
			ResultSet result = consulta.executeQuery();
			while (result.next()) {
				ItemDeCompraDao dao = ItemDeCompraDao.getInstance(); 

				Compra obj = new Compra();
				obj.setId(result.getInt("idcompra"));
				obj.setData(result.getDate("data"));
				
				// Localiza todos os Itens de Compra
				List<ItemDeCompra> itens = dao.listar(obj.getId());
				obj.setItens(itens);
				
				compras.add(obj);
			}
			return compras;
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao listar os Compras");
		}		
	}
}
