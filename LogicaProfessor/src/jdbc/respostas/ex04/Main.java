package jdbc.respostas.ex04;

import java.awt.EventQueue;

import jdbc.respostas.ex04.view.TelaCompra;

public class Main {	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCompra frame = new TelaCompra();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
