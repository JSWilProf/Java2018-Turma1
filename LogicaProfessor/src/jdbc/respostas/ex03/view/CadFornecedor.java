package jdbc.respostas.ex03.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import jdbc.respostas.ex03.model.Endereco;
import jdbc.respostas.ex03.model.Fornecedor;
import jdbc.respostas.ex03.model.Produto;
import jdbc.respostas.ex03.model.dao.DaoException;
import jdbc.respostas.ex03.model.dao.DatabaseManager;
import jdbc.respostas.ex03.model.dao.FornecedorDao;
import jdbc.respostas.ex03.view.adapter.ProdutoAdapter;

@SuppressWarnings("serial")
public class CadFornecedor extends JFrame implements ActionListener, MouseListener {
	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblEnd;
	private JButton btEndereco;
	private JPanel panel;
	private JButton btAdiciona;
	private JButton btExclui;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btSalvar;
	private JButton btListar;
	private JButton btFechar;
	
	private JPopupMenu menu = new JPopupMenu();
	
	private List<Produto> produtos = new ArrayList<>();
	private ProdutoAdapter model = new ProdutoAdapter(produtos);
	private Endereco endereco = new Endereco();

	/*
	 * Caso esta aplicação deva editar um Fornecedor previamente cadastrado
	 * deverá ser criado um médoto para buscar o fornecedor através do
	 * FornecedorDao no médoto localizar.
	 * 
	 * Este método carregará os dados de Fornecedor seu Endereço e todos
	 * os Produtos que este destribui.
	 * 
	 * Uma vez tenha rececibo o Fornecedor do Dao, suas informações deverão
	 * ser carregadas nos "List<Produto> produtos" e "Endereco endereco" na
	 * inicialização da aplicação.
	 * 
	 * Por fim o construtor deverá ser modificado para aceitar um argumento
	 * do tipo "int" onde será passado o "ID" do Fornecedor a ter seus dados
	 * editados.
	 */
	
	public CadFornecedor() {
		setTitle("Cadastro de Fornecedores");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 468, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		lblEnd = new JLabel("End.");
		
		btEndereco = new JButton("...");
		btEndereco.addActionListener(this);
		btEndereco.setToolTipText("Abre o Diálogo de cadastro de Entedeço");
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Produtos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		btSalvar = new JButton("Salvar");
		btSalvar.addActionListener(this);
		
		btFechar = new JButton("Fechar");
		btFechar.addActionListener(this);
		
		btListar = new JButton("Listar");
		btListar.addActionListener(this);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblEnd)
								.addComponent(lblNome))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btEndereco)
								.addComponent(tfNome, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(btSalvar)
								.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btListar)
								.addGap(90)
								.addComponent(btFechar))
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 432, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNome))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btEndereco)
						.addComponent(lblEnd))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btSalvar)
						.addComponent(btFechar)
						.addComponent(btListar))
					.addContainerGap(12, Short.MAX_VALUE))
		);
		
		btAdiciona = new JButton("+");
		btAdiciona.setToolTipText("Incluir Produto");
		btAdiciona.addActionListener(this);
		
		btExclui = new JButton("-");
		btExclui.setToolTipText("Excluir Produto");
		btExclui.addActionListener(this);
		
		scrollPane = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btAdiciona)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btExclui)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btAdiciona)
						.addComponent(btExclui))
					.addContainerGap())
		);

		table = new JTable();
		table.addMouseListener(this);
		table.setModel(model);
		table.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(table);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		
		getRootPane().setDefaultButton(btSalvar);
		setLocationRelativeTo(null);
		
		configurarMenu();
	}
	
	private void configurarMenu() {
		JMenuItem item = new JMenuItem("Editar");
		item.setActionCommand("menuEditar");
		item.addActionListener(this);
		menu.add(item);
		
		item = new JMenuItem("Excluir");
		item.addActionListener(this);
		item.setActionCommand("menuExcluir");
		menu.add(item);

	}
	
	private int getLinhaSelecionada() {
		int linha = table.getSelectedRow();
		if(linha > -1) {
			if(table.getRowSorter() != null) {
				linha = table.getRowSorter().convertRowIndexToModel(linha);
			}
		}
		return linha;
	}

	private void editaProduto() {
		editaProduto(null);
	}

	private void editaProduto(Produto prod) {
		CadProduto dialog = new CadProduto(prod);
		dialog.setLocationRelativeTo(this);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
		
		if(!dialog.isCancelado()) {
			Produto produto = dialog.getProduto();
			model.adicionar(produto);
		}
	}
	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		
		try {
			if(evento.getActionCommand().equals("menuEditar")) {
				int linha = getLinhaSelecionada();
				if(linha > -1) {
					editaProduto(model.localizar(linha));
				} else {
					JOptionPane.showMessageDialog(this, "Selecione um Produto para ser excluido!");
				}
			} else if(botao.equals(btEndereco)) {
				CadEndereco dialog = new CadEndereco(endereco);
				dialog.setLocationRelativeTo(this);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);			
			} else if(botao.equals(btAdiciona)) {
				editaProduto();
			} else if(botao.equals(btExclui) || evento.getActionCommand().equals("menuExcluir")) {
				int linha = getLinhaSelecionada();
				if(linha > -1) {
					model.excluir(linha);
				} else {
					JOptionPane.showMessageDialog(this, "Selecione um Produto para ser excluido!");
				}
			} else if(botao.equals(btSalvar)) {
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setNome(tfNome.getText());
				fornecedor.setEndereco(endereco);
				fornecedor.setProdutos(produtos);
				produtos.stream()
					.filter(p -> p.getId() < 0)			// para todos os Produtos Novos
					.forEach((p) -> {
						p.setId(null);				   	// faça incluir do Banco de Dados
						p.setFornecedor(fornecedor);	// vincule ao Fornecedor
					});
				
				FornecedorDao dao = FornecedorDao.getInstance();
				dao.salvar(fornecedor);
				
				tfNome.setText("");
				endereco = new Endereco();
				produtos = new ArrayList<>();
				model = new ProdutoAdapter(produtos);
				table.setModel(model);
				tfNome.requestFocus();
			} else if(botao.equals(btListar)) {
				FornecedorDao dao = FornecedorDao.getInstance();
				JOptionPane.showMessageDialog(this, 
						"Cadastro de Fornecedores\n\n" + 
						dao.listar().stream()
							.map((f) -> f.toString())
							.collect(Collectors.joining("\n")));
			} else {
				DatabaseManager.closeConnection();
				System.exit(0);
			}
		} catch (DaoException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage());
		}
	}
	
	public void mouseClicked(MouseEvent ev) {
		if(ev.getButton() ==  MouseEvent.BUTTON3) {
			int linha = table.getSelectedRow();
			if(linha > -1) {
				if(table.getRowSorter() != null) {
					linha = table.getRowSorter().convertRowIndexToModel(linha);
				}
				menu.show(table, ev.getX(), ev.getY());
			} else {
				JOptionPane.showMessageDialog(this, "Um Produto precisa estar selecionado");
			}
		}
	}
	
	public void mouseEntered(MouseEvent e) {
	}
	
	public void mouseExited(MouseEvent e) {
	}
	
	public void mousePressed(MouseEvent e) {
	}
	
	public void mouseReleased(MouseEvent e) {
	}
}
