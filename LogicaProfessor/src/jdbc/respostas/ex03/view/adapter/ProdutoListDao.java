package jdbc.respostas.ex03.view.adapter;

import java.util.List;
import java.util.stream.Collectors;

import jdbc.respostas.ex03.model.Produto;

public class ProdutoListDao {
	public static ProdutoListDao instance; 
	private List<Produto> lista;
	private int indice = -1;
	
	private ProdutoListDao(List<Produto> lista) {
		this.lista = lista;
	}
	
	public static ProdutoListDao getInstance(List<Produto> lista) {
		if(instance == null) {
			instance = new ProdutoListDao(lista);
		}
		
		return instance;
	}
	
	public void salvar(Produto prod) {
		if(prod.getId() == null) { // Incluir
			prod.setId(indice--);
			lista.add(prod);
		} else {                   // Atualizar
			Produto obj = localizar(prod.getId());
			if(obj != null) {
				obj.setNome(prod.getNome());
				obj.setFornecedor(prod.getFornecedor());
				obj.setPreco(prod.getPreco());
			}
		}
	}
	
	public Produto localizar(int id) {
		return lista.stream()   				// Procure na lista de produtos
			.filter(prod -> prod.getId() == id) // qualquer produto que tenha este ID
			.findFirst()					    // separe o 1º que você encontrar
			.orElse(null);						// e me devolva, caso contrario retorne NULL
	}
	
	public void remover(int id) {
		lista.removeIf(prod -> prod.getId() == id);
	}

	public List<Integer> listar() {
		return lista.stream()					// Para cada Produto na lista
				.map(prod -> prod.getId())		// separe o ID
				.collect(Collectors.toList());  // e crie um List pra mim
	}
}
