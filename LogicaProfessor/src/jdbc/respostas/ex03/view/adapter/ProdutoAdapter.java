package jdbc.respostas.ex03.view.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import jdbc.respostas.ex03.model.Produto;


@SuppressWarnings("serial")
public class ProdutoAdapter extends AbstractTableModel {
	private ProdutoListDao dao;
	private String[] titulo = { "Nome" , "Preço" };
	private Map<Integer, Integer> mapa;
	
	
	public ProdutoAdapter(List<Produto> lista) {
		 dao = ProdutoListDao.getInstance(lista);
		 criaMapa();
	}

	private void criaMapa() {
		mapa = new HashMap<>();
		List<Integer> lista= dao.listar();
		for(int linha = 0;linha < lista.size();linha++) {
			mapa.put(linha, lista.get(linha));
		}
		fireTableDataChanged();
	}
	
	public void adicionar(Produto produto) {
		dao.salvar(produto);
		criaMapa();
	}

	public void excluir(int linha) {
		dao.remover(mapa.get(linha));
		criaMapa();
	}

	public Produto localizar(int linha) {
		return dao.localizar(mapa.get(linha));
	}

	@Override
	public int getRowCount() {
		return mapa.size();
	}

	@Override
	public int getColumnCount() {
		return titulo.length;
	}

	@Override
	public String getColumnName(int col) {
		return titulo[col];
	}
	
	@Override
	public Object getValueAt(int linha, int col) {
		Object valor = null;
		Produto obj = dao.localizar(mapa.get(linha));
		
		switch (col) {
		case 0:
			valor = obj.getNome();
			break;
		case 1:
			valor = String.format("%,.2f", obj.getPreco());
			break;
		}
		return valor;
	}
}
