package jdbc.respostas.ex03.model.dao;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String msg) {
		super(msg);
	}
}
