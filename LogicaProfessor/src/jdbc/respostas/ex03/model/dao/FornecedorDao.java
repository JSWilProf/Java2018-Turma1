package jdbc.respostas.ex03.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.respostas.ex03.model.Endereco;
import jdbc.respostas.ex03.model.Fornecedor;
import jdbc.respostas.ex03.model.Produto;

public class FornecedorDao {
	private static FornecedorDao instance;
	private Connection con;
	private PreparedStatement insere;
	private PreparedStatement altera;
	private PreparedStatement remove;
	private PreparedStatement consulta;
	private PreparedStatement queryId;	

	private FornecedorDao() throws DaoException {
		try {
			con = DatabaseManager.getConnection();
	
			consulta = con.prepareStatement("SELECT * FROM fornecedor");
			insere = con.prepareStatement("INSERT INTO fornecedor (nome, idendereco) VALUES (?, ?)");
			altera = con.prepareStatement("UPDATE fornecedor set nome=?, idendereco=? WHERE idfornecedor=?");
			remove = con.prepareStatement("DELETE fornecedor WHERE idfornecedor=?");
			queryId = con.prepareStatement("select last_insert_id() as id"); 
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("A conexão com o Banco de Dados foi estabelecida");
		}
	}
	
	public static FornecedorDao getInstance() throws DaoException {
		if(instance == null) {
			instance = new FornecedorDao();
		}
		
		return instance;
	}
	
	private void inserir(Fornecedor forn) throws SQLException, DaoException {
		// Salva o Endereço
		Endereco end = EnderecoDao.getInstance().salvar(forn.getEndereco());
		forn.getEndereco().setId(end.getId());
		
		// Salva o Fornecedor
		insere.setString(1, forn.getNome());
		insere.setInt(2, end.getId());
		insere.execute();

		ResultSet result = queryId.executeQuery();
		if(result.next()) {
			forn.setId(result.getInt("id")); 
		} else {
			con.rollback();
			con.setAutoCommit(true);
			throw new DaoException("Falha ao salver o Fornecedor");
		}
		
		// Salva os Produtos
		ProdutoDao dao = ProdutoDao.getInstance();
		for(Produto prod : forn.getProdutos()) {
			prod.setFornecedor(forn);
			dao.salvar(prod);
		}	
	}
	
	private void atualizar(Fornecedor forn) throws SQLException, DaoException {
		// Atualizar o Endereço
		EnderecoDao.getInstance().salvar(forn.getEndereco());
		
		// Atualiza o Fornecedor
		altera.setString(1, forn.getNome());
		altera.setInt(2, forn.getEndereco().getId());
		altera.setInt(3, forn.getId());
		altera.execute();
		
		// Atualiza os Produtos
		ProdutoDao dao = ProdutoDao.getInstance();
		for(Produto prod : forn.getProdutos()) {
			prod.setFornecedor(forn);
			dao.salvar(prod);
		}	
	}
	
	public Fornecedor salvar(Fornecedor forn) throws DaoException {
		String acao = "incluir";
		try {
			con.setAutoCommit(false);

			if(forn.getId() == null) { // Incluir
				inserir(forn);	
				con.commit();
			} else {                   // Atualizar
				acao = "atualizar";
				atualizar(forn);
				con.commit();
			}
			return forn;
		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				con.rollback();
				con.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new DaoException("Falha ao " + acao + " o Fornecedor");					
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void remover(int id) throws DaoException {
		try {
			// Foi configurado cascade on delete no Banco de Dados
			// assim ao excluir um Fornecedor todos os Produtos 
			// serão excluidos também
			remove.setInt(1, id);
			remove.execute();			
		} catch (SQLException ex) {
			throw new DaoException("Falha ao excluir o Fornecedor");
		}		
	}

	public List<Fornecedor> listar() throws DaoException {
		try {
			List<Fornecedor> fornecedores = new ArrayList<>();
			
			ResultSet result = consulta.executeQuery();
			while (result.next()) {
				EnderecoDao daoEndereco = EnderecoDao.getInstance(); 
				ProdutoDao daoProduto = ProdutoDao.getInstance(); 

				Fornecedor obj = new Fornecedor();
				obj.setId(result.getInt("idfornecedor"));
				obj.setNome(result.getString("nome"));
				
				// Localiza o Endereco
				int idEnd = result.getInt("idendereco");
				obj.setEndereco(daoEndereco.localizar(idEnd));
				
				// Localiza todos os Produtos
				List<Produto> produtos = new ArrayList<>();
				for (Produto prod : daoProduto.listar(obj.getId())) {
					prod.setFornecedor(obj);
					produtos.add(prod);
				}
				obj.setProdutos(produtos);
				
				fornecedores.add(obj);
			}
			return fornecedores;
		} catch (SQLException ex) {
			throw new DaoException("Falha ao listar os Fornecedores");
		}		
	}
}
