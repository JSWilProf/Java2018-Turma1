package jdbc.respostas.ex03.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.respostas.ex03.model.Endereco;

public class EnderecoDao {
	private static EnderecoDao instance;
	private Connection con;
	private PreparedStatement insere;
	private PreparedStatement altera;
	private PreparedStatement consulta;
	private PreparedStatement localiza;
	private PreparedStatement queryId;

	private EnderecoDao() throws DaoException {
		try {
			con = DatabaseManager.getConnection();
	
			consulta = con.prepareStatement("SELECT * FROM endereco");
			localiza = con.prepareStatement("SELECT * FROM endereco WHERE idendereco=?");
			insere = con.prepareStatement("INSERT INTO endereco (logradouro, numero, bairro, cep) VALUES (?, ?, ?, ?)");
			altera = con.prepareStatement("UPDATE endereco set logradouro=?, numero=?, bairro=?, cep=? WHERE idendereco=?");
			queryId = con.prepareStatement("select last_insert_id() as id"); 
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("A conexão com o Banco de Dados foi estabelecida");
		}
	}
	
	public static EnderecoDao getInstance() throws DaoException {
		if(instance == null) {
			instance = new EnderecoDao();
		}
		
		return instance;
	}

	public Endereco salvar(Endereco obj) throws DaoException {
		String acao = "incluir";
		try {
			if(obj.getId() == null) {
				insere.setString(1, obj.getLogradouro());
				insere.setString(2, obj.getNumero());
				insere.setString(3, obj.getBairro());
				insere.setString(4, obj.getCep());
				insere.execute();
				
				ResultSet result = queryId.executeQuery();
				if(result.next()) {
					obj.setId(result.getInt("id")); 
				} else {
					throw new DaoException("Falha ao salver o Endreço");
				}
			} else {
				acao = "atualizar";
				altera.setString(1, obj.getLogradouro());
				altera.setString(2, obj.getNumero());
				altera.setString(3, obj.getBairro());
				altera.setString(4, obj.getCep());
				altera.setInt(5, obj.getId());
				altera.execute();				
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao " + acao + " o Endreço");
		}
		return obj;
	}

	public Endereco localizar(int id) throws DaoException {
		try {
			localiza.setInt(1, id);
			ResultSet result = localiza.executeQuery();
			if (result.next()) {
				return buildObject(result);
			} else {
				throw new DaoException("O Endereço não foi encontrado");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao listar os Endreços");
		}
	}

	public List<Endereco> listar() throws DaoException {
		try {
			List<Endereco> lista = new ArrayList<>();
	
			ResultSet result = consulta.executeQuery();
			while (result.next()) {
				lista.add(buildObject(result));
			}
	
			return lista;
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao listar os Endreços");
		}
	}

	private Endereco buildObject(ResultSet result) throws SQLException {
		Endereco obj = new Endereco();
		obj.setId(result.getInt("idendereco"));
		obj.setLogradouro(result.getString("logradouro"));
		obj.setNumero(result.getString("numero"));
		obj.setBairro(result.getString("bairro"));
		obj.setCep(result.getString("cep"));
		return obj;
	}
}
