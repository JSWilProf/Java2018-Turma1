package jdbc.respostas.ex03.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.respostas.ex03.model.Produto;

public class ProdutoDao {
	private static ProdutoDao instance;
	private Connection con;
	private PreparedStatement insere;
	private PreparedStatement altera;
	private PreparedStatement remove;
	private PreparedStatement consulta;
	private PreparedStatement queryId;	

	private ProdutoDao() throws DaoException {
		try {
			con = DatabaseManager.getConnection();
	
			consulta = con.prepareStatement("SELECT * FROM produto where idfornecedor=?");
			insere = con.prepareStatement("INSERT INTO produto (nome, preco, idfornecedor) VALUES (?, ?, ?)");
			altera = con.prepareStatement("UPDATE produto set nome=?, preco=?, idfornecedor=? WHERE idproduto=?");
			remove = con.prepareStatement("DELETE produto WHERE idproduto=?");
			queryId = con.prepareStatement("select last_insert_id() as id"); 
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("A conexão com o Banco de Dados foi estabelecida");
		}
	}
	
	public static ProdutoDao getInstance() throws DaoException {
		if(instance == null) {
			instance = new ProdutoDao();
		}
		
		return instance;
	}
	
	public Produto salvar(Produto prod) throws DaoException {
		String acao = "incluir";
		try {
			if(prod.getId() == null) { // Incluir
				insere.setString(1, prod.getNome());
				insere.setDouble(2, prod.getPreco());
				insere.setInt(3, prod.getFornecedor().getId());
				insere.execute();
				
				ResultSet result = queryId.executeQuery();
				if(result.next()) {
					prod.setId(result.getInt("id")); 
				} else {
					throw new DaoException("Falha ao salvar o Produto");
				}
			} else {                   // Atualizar
				acao = "atualizar";
				altera.setString(1, prod.getNome());
				altera.setDouble(2, prod.getPreco());
				altera.setInt(3, prod.getFornecedor().getId());
				altera.setInt(4, prod.getId());
				altera.execute();
			}
			return prod;
		} catch (SQLException ex) {
			throw new DaoException("Falha ao " + acao + " o Produto");
		}
	}
	
	public void remover(int id) throws DaoException {
		try {
			remove.setInt(1, id);
			remove.execute();
		} catch (SQLException ex) {
			throw new DaoException("Falha ao excluir o Produto");
		}		
	}

	public List<Produto> listar(int idFornecedor) throws DaoException {
		try {
			List<Produto> lista = new ArrayList<>();
			
			consulta.setInt(1, idFornecedor);
			ResultSet result = consulta.executeQuery();
			while (result.next()) {
				Produto prod = new Produto();
				prod.setId(result.getInt("idproduto"));
				prod.setNome(result.getString("nome"));
				prod.setPreco(result.getDouble("preco"));
				lista.add(prod);
			}
			return lista;
		} catch (SQLException ex) {
			throw new DaoException("Falha ao listar os Produtos");
		}		
	}
}
