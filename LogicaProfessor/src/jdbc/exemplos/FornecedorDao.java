package jdbc.exemplos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * 
 * DAO - Data Access Objeto
 * 
 * Uma classe especializada em lidar com o acesso aos dados em Banco de Dados
 * ocultando os SQLs da aplicação para uma simplificação da utilização deste
 * recurso.
 * 
 */

public class FornecedorDao {
	private Connection con;
	private PreparedStatement insere;
	private PreparedStatement consulta;

	public FornecedorDao() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://localhost:3307/prjgn1801", "root", "root132");

		consulta = con.prepareStatement("SELECT * FROM fornecedor");
		insere = con.prepareStatement("INSERT INTO fornecedor (nome , endereco) VALUES (?, ?)");
	}
	
	public void inserir(Fornecedor obj) throws SQLException {
		insere.setString(1, obj.getNome());
		insere.setString(2, obj.getEndereco());
		insere.execute();
	}
	
	public List<Fornecedor> listar() throws SQLException {
		List<Fornecedor> lista = new ArrayList<>();
		
		ResultSet result = consulta.executeQuery();
		while(result.next()) {
			Fornecedor obj = new Fornecedor();
			obj.setId(result.getInt("idfornecedor"));
			obj.setNome(result.getString("nome"));
			obj.setEndereco(result.getString("endereco"));
			
			lista.add(obj);
		}
		
		return lista;
	}

	public void fechar() {
		try {
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
