package jdbc.exemplos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class CadastroDeFornecedores {
	public static void main(String[] args) {
		Connection con;
		PreparedStatement insForn = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3307/prjgn1801", "root", "root132");
			
			 insForn = con.prepareStatement("INSERT INTO fornecedor (nome , endereco) VALUES (?, ?)"); 
		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "O Driver JDBC não foi encontrado");
			ex.printStackTrace();
			System.exit(0);
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "Houve problemas ao conectar no Banco de Dados");
			ex.printStackTrace();
			System.exit(0);
		}
		
		String nome = JOptionPane.showInputDialog("Informe o Nome");
		while(!nome.isEmpty()) {
			String endereco = JOptionPane.showInputDialog("Informe o Endereço");
			try {
				
//				Fornecedor fornecedor = new Fornecedor();
//				fornecedor.setNome(nome);
//				fornecedor.setEndereco(endereco);
			
				insForn.setString(1, nome);
				insForn.setString(2, endereco);
				insForn.execute();
				
				nome = JOptionPane.showInputDialog("Informe o Nome");
			} catch (SQLException ex) {
				JOptionPane.showMessageDialog(null, "Houve problemas ao gravar um Fornecedor");
				ex.printStackTrace();
			}
		}
	}
}
